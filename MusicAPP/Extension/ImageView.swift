//
//  ImageView.swift
//  mvp
//
//  Created by Николай on 27.10.21.
//

import UIKit

extension UIImageView {
    func setCustomImage(_ imgURLString: String?) {
        guard let imageURLString = imgURLString else {
            self.image = ImageNameStruct.defaultImage
            return
        }
        DispatchQueue.global().async { [weak self] in
            let data = try? Data(contentsOf: URL(string: imageURLString)!)
            DispatchQueue.main.async {
                self?.image = data != nil ? UIImage(data: data ?? Data()) :ImageNameStruct.defaultImage
            }
        }
    }
}
