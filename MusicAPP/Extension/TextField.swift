//
//  TextField.swift
//  music
//
//  Created by Николай on 14.10.21.
//

import UIKit

extension UITextField {
    func leftView(_ image: UIImage?, imageWidth: CGFloat, padding: CGFloat) {
        let imageView = UIImageView(image: image)
        imageView.frame = CGRect(x: padding, y: 0, width: imageWidth, height: frame.height)
        imageView.contentMode = .center
        let containerView = UIView(frame: CGRect(x: 0, y: 0, width: imageWidth + 1.5 * padding, height: frame.height))
        containerView.addSubview(imageView)
        leftView = containerView
        leftViewMode = .always
    }
}
