//
//  TabBar.swift
//  mvp
//
//  Created by Николай on 27.10.21.
//

import Foundation

extension TabBarViewController {
    func setupTabBar() {
        tabBar.tintColor = ColorStruct.buttonSignIn
        tabBar.isTranslucent = false
        tabBar.barTintColor = ColorStruct.viewBackground
    }
}
