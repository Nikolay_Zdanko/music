//
//  AlbumDetailsTableViewCell.swift
//  MusicAPP
//
//  Created by Егор Евсеенко on 29.10.2021.
//

import UIKit

class AlbumSongsTableViewCell: UITableViewCell {
    // MARK: - Variable
    
    static let identifier = "DiscoverTableViewCell"
    
    lazy var tableImageView: UIImageView = {
        var imageView = UIImageView()
        imageView.layer.cornerRadius = DimensionsStruct.cornerRadius8
        imageView.clipsToBounds = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    lazy var trackLabel: UILabel = {
        var label = UILabel()
        label.textAlignment = .left
        label.sizeToFit()
        label.textColor = ColorStruct.forgotLabel
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    lazy var artistLabel: UILabel = {
        var label = UILabel()
        label.textAlignment = .left
        label.sizeToFit()
        label.textColor = ColorStruct.textFieldPlaceholder
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    lazy var stackViewVertical: UIStackView = {
        var stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .fillEqually
        stackView.spacing = DimensionsStruct.spacingStackVertical
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.addArrangedSubview(trackLabel)
        stackView.addArrangedSubview(artistLabel)
        return stackView
    }()
    lazy var timeSongLabel: UILabel = {
       var label = UILabel()
        label.textColor = ColorStruct.textFieldPlaceholder
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    lazy var buttonForTableView: UIButton = {
        var button = UIButton()
        button.setImage(ImageNameStruct.buttonTable, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    // MARK: - Lifecycle function
        override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
            super.init(style: style, reuseIdentifier: "DiscoverTableViewCell")
            contentView.addSubview(tableImageView)
            contentView.addSubview(stackViewVertical)
            contentView.addSubview(timeSongLabel)
            contentView.addSubview(buttonForTableView)
            contentView.backgroundColor = ColorStruct.viewBackground
        }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        setupContraintForImage()
        setupContraintForStackView()
        setupContraintForButton()
        setupConstraintForTimeLabel()
    }
    
    private func setupContraintForImage() {
        tableImageView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 24).isActive = true
        tableImageView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 8).isActive = true
        tableImageView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -8).isActive = true
        tableImageView.widthAnchor.constraint(equalToConstant: 41).isActive = true
        tableImageView.heightAnchor.constraint(equalToConstant: 41).isActive = true
    }
    private func setupContraintForStackView() {
        stackViewVertical.leadingAnchor.constraint(equalTo: tableImageView.trailingAnchor, constant: 16).isActive = true
        stackViewVertical.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 9).isActive = true
        stackViewVertical.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -8).isActive = true
        stackViewVertical.widthAnchor.constraint(equalToConstant: 205).isActive = true
    }
    
    private func setupContraintForButton() {
        buttonForTableView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -24).isActive = true
        buttonForTableView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 18).isActive = true
        buttonForTableView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -19).isActive = true
        buttonForTableView.widthAnchor.constraint(equalToConstant: contentView.frame.height / 2.2).isActive = true
        buttonForTableView.heightAnchor.constraint(equalToConstant: contentView.frame.height / 2.2).isActive = true
    }
    
    private func setupConstraintForTimeLabel() {
        timeSongLabel.trailingAnchor.constraint(equalTo: buttonForTableView.leadingAnchor, constant: -16).isActive = true
        timeSongLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 18).isActive = true
        timeSongLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -19).isActive = true
    }
}

