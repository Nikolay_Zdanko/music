//
//  AlbumDetailsView.swift
//  MusicAPP
//
//  Created by Егор Евсеенко on 29.10.2021.
//

import UIKit
import Kingfisher

class AlbumDetailsViewController: UIViewController{
    
    //MARK: - UI elements in code
    var presenter: AlbumDetailsPresenterProtocol!
    
    lazy var scrollView: UIScrollView = {
        var scrollView = UIScrollView(frame: .zero)
        scrollView.isScrollEnabled = true
        scrollView.showsVerticalScrollIndicator = false
        let screensize: CGRect = UIScreen.main.bounds
        let screenWidth = screensize.width
        let screenHeight = screensize.height
        scrollView.frame = CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight)
        scrollView.contentSize = CGSize(width: scrollView.frame.width, height: scrollView.frame.height)
        return scrollView
    }()
    
    lazy var avatarImageView: UIImageView = {
        var avatarImageView = UIImageView()
        avatarImageView.layer.cornerRadius = AlbumsCollectionConstants.albumImageRadius
        avatarImageView.translatesAutoresizingMaskIntoConstraints = false
        avatarImageView.layer.masksToBounds = true
        return avatarImageView
    }()
    
    lazy var playButton: UIButton = {
        var buttonAdd = UIButton()
        buttonAdd.setImage(ImageNameStruct.playButton, for: .normal)
        buttonAdd.translatesAutoresizingMaskIntoConstraints = false
        return buttonAdd
    }()
    
    lazy var albumNameLabel: UILabel = {
        var albumNameLabel = UILabel()
        albumNameLabel.font = FontSctuct.bold14
        albumNameLabel.textColor = ColorStruct.forgotLabel
        albumNameLabel.textAlignment = .left
        albumNameLabel.sizeToFit()
        albumNameLabel.translatesAutoresizingMaskIntoConstraints = false
        return albumNameLabel
    }()
    
    lazy var artistOfAlbumLabel: UILabel = {
        var artistOfAlbumLabel = UILabel()
        artistOfAlbumLabel.font = FontSctuct.regular14
        artistOfAlbumLabel.textColor = ColorStruct.artiscOfAlbum
        artistOfAlbumLabel.textAlignment = .left
        artistOfAlbumLabel.sizeToFit()
        artistOfAlbumLabel.translatesAutoresizingMaskIntoConstraints = false
        return artistOfAlbumLabel
    }()
    
    lazy var albumTextDescription: UIStackView = {
        var albumDescription = UIStackView()
        albumDescription.axis = .vertical
        albumDescription.distribution = .fillEqually
        albumDescription.spacing = 0
        albumDescription.translatesAutoresizingMaskIntoConstraints = false
        albumDescription.addArrangedSubview(albumNameLabel)
        albumDescription.addArrangedSubview(artistOfAlbumLabel)
        return albumDescription
    }()
    
    lazy var likeButton: UIButton = {
        var likeButton = UIButton()
        likeButton.setImage(ImageNameStruct.emptyLike, for: .normal)
        likeButton.translatesAutoresizingMaskIntoConstraints = false
        return likeButton
    }()
    
    lazy var albumDescription: UIStackView = {
        var albumDescription = UIStackView()
        albumDescription.axis = .horizontal
        albumDescription.spacing = 0
        albumDescription.translatesAutoresizingMaskIntoConstraints = false
        albumDescription.addArrangedSubview(albumTextDescription)
        albumDescription.addArrangedSubview(likeButton)
        return albumDescription
    }()
    
    lazy var viewForTable: UIStackView = {
        let view = UIStackView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var viewForScroll: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    
    lazy var headerForTableView: UIView = {
        let headerView = UIView()
        headerView.translatesAutoresizingMaskIntoConstraints = false
        headerView.backgroundColor = .green
        return headerView
    }()
    
    lazy var headerLabel: UILabel = {
        let label = UILabel()
        label.text = AlbumDetailsText.contains
        label.font = FontSctuct.bold18
        label.textColor = ColorStruct.albumDetailsHeader
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    lazy var albumTableView: UITableView = {
        var tableView = UITableView()
        tableView.register(AlbumSongsTableViewCell.self,
                           forCellReuseIdentifier: AlbumSongsTableViewCell.identifier)
        tableView.separatorStyle = .singleLine
        tableView.separatorColor = ColorStruct.textFieldPlaceholder
        tableView.backgroundColor = .clear
        tableView.separatorInset = DimensionsStruct.separatorTableView
        tableView.tableFooterView = UIView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.isScrollEnabled = false
        tableView.translatesAutoresizingMaskIntoConstraints = false
        return tableView
    }()
    
    //MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        addSubViewForView()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        view.backgroundColor = ColorStruct.viewBackground
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        setUpScrollConstraints()
        setupConstraintForAvatarImageView()
        setupConstraintForButtonAdd()
        setupConstraintForAlbumDescription()
        setUpHeader()
        sutUpTableViewForAlbumSongs()
    }
    
    
    private func addSubViewForView() {
        view.addSubview(scrollView)
        scrollView.addSubview(viewForScroll)
        viewForScroll.addSubview(avatarImageView)
        viewForScroll.addSubview(playButton)
        viewForScroll.addSubview(albumDescription)
        viewForScroll.addSubview(viewForTable)
        //        viewForTable.addSubview(headerForTableView)
        viewForTable.addSubview(albumTableView)
        albumTableView.tableHeaderView = headerForTableView
    }
    
    private func convertSecondsToHrMinuteSec(seconds:Int) -> String{
        let formatter = DateComponentsFormatter()
        formatter.allowedUnits = [.hour, .minute, .second]
        formatter.unitsStyle = .positional
        let formattedString = formatter.string(from:TimeInterval(seconds))!
        return formattedString
    }
    
    private func setUpScrollConstraints() {
        scrollView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: view.topAnchor).isActive = true
        scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        scrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        
        
        viewForScroll.topAnchor.constraint(equalTo: scrollView.topAnchor).isActive = true
        viewForScroll.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor).priority = .defaultLow
        viewForScroll.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor , constant: -100).isActive = true //
        viewForScroll.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor).isActive = true
        viewForScroll.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor).isActive = true
        viewForScroll.heightAnchor.constraint(equalTo: scrollView.heightAnchor).priority = .defaultLow
        viewForScroll.heightAnchor.constraint(greaterThanOrEqualTo: scrollView.heightAnchor).isActive = true
        viewForScroll.widthAnchor.constraint(equalTo: scrollView.widthAnchor).isActive = true
    }
    
    private func setupConstraintForAvatarImageView() {
        avatarImageView.centerXAnchor.constraint(equalTo: viewForScroll.centerXAnchor).isActive = true
        avatarImageView.topAnchor.constraint(equalTo: viewForScroll.topAnchor, constant: 40).isActive = true
        avatarImageView.heightAnchor.constraint(equalToConstant: view.frame.width / 2).isActive = true
        avatarImageView.widthAnchor.constraint(equalToConstant: view.frame.width / 2).isActive = true
    }
    
    private func setupConstraintForButtonAdd() {
        playButton.centerYAnchor.constraint(equalTo: avatarImageView.bottomAnchor, constant: -5).isActive = true
        playButton.centerXAnchor.constraint(equalTo: avatarImageView.trailingAnchor, constant: -5).isActive = true
        playButton.widthAnchor.constraint(equalToConstant: 50).isActive = true
        playButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
    }
    
    private func setupConstraintForAlbumDescription() {
        likeButton.widthAnchor.constraint(equalToConstant: 20).isActive = true
        albumDescription.topAnchor.constraint(equalTo: avatarImageView.bottomAnchor,constant: 50).isActive = true
        albumDescription.leadingAnchor.constraint(equalTo: viewForScroll.leadingAnchor,constant: 24).isActive = true
        albumDescription.trailingAnchor.constraint(equalTo: viewForScroll.trailingAnchor, constant: -24).isActive = true
        albumDescription.heightAnchor.constraint(equalToConstant: 50).isActive = true
    }
    
    private func setUpHeader(){
        let header = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 25))
        header.backgroundColor = .clear
        header.addSubview(headerLabel)
        headerLabel.frame = header.bounds
        headerLabel.leadingAnchor.constraint(equalTo: header.leadingAnchor, constant: 24).isActive = true
        albumTableView.tableHeaderView = header
    }
    
    private func sutUpTableViewForAlbumSongs() {
        viewForTable.heightAnchor.constraint(equalTo: viewForScroll.heightAnchor).priority = .defaultLow
        viewForTable.heightAnchor.constraint(greaterThanOrEqualToConstant: albumTableView.contentSize.height).isActive = true
        viewForTable.topAnchor.constraint(equalTo: albumDescription.bottomAnchor,constant: 24).isActive = true
        viewForTable.bottomAnchor.constraint(equalTo: viewForScroll.bottomAnchor).isActive = true
        viewForTable.leadingAnchor.constraint(equalTo: viewForScroll.leadingAnchor).isActive = true
        viewForTable.trailingAnchor.constraint(equalTo: viewForScroll.trailingAnchor).isActive = true
        
        albumTableView.topAnchor.constraint(equalTo: viewForTable.topAnchor).isActive = true
        albumTableView.bottomAnchor.constraint(equalTo: viewForScroll.bottomAnchor).isActive = true
        albumTableView.leadingAnchor.constraint(equalTo: viewForTable.leadingAnchor).isActive = true
        albumTableView.trailingAnchor.constraint(equalTo: viewForTable.trailingAnchor).isActive = true
        
    }
}

//MARK: - TableView
extension AlbumDetailsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}

extension AlbumDetailsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.numberOfRowsInSection()
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DiscoverTableViewCell") as! AlbumSongsTableViewCell
        if let albums = presenter.albums {
            let albumImage = presenter.album?.cover_xl
            let album = presenter.albums?.data[indexPath.row]
            let url = URL(string: albumImage ?? "")
            cell.trackLabel.text = album?.title
            cell.artistLabel.text = album?.artist.name
            cell.timeSongLabel.text = convertSecondsToHrMinuteSec(seconds: album?.duration ?? 0)
            cell.tableImageView.kf.setImage(with: url)
            return cell
        }
        if let playlist = presenter.playlists {
            cell.artistLabel.text =  playlist.data[indexPath.row].artist.name
            cell.trackLabel.text = playlist.data[indexPath.row].title
            let url = URL(string: playlist.data[indexPath.row].album.cover_xl)
            cell.tableImageView.kf.setImage(with: url)
            cell.timeSongLabel.text = convertSecondsToHrMinuteSec(seconds: playlist.data[indexPath.row].duration )
            return cell
        }
        return cell
    }
}

extension AlbumDetailsViewController: AlbumDetailsViewProtocol {
    
    func setPlaylistInfo(playlist: PlaylistsData){
        let url = URL(string: playlist.picture_big ?? "")
        avatarImageView.kf.setImage(with: url)
        albumNameLabel.text = playlist.title
        artistOfAlbumLabel.isHidden = true
    }
    
    func setAlbumInfo(album: AlbumsData) {
        let url = URL(string: album.cover_xl ?? "")
        avatarImageView.kf.setImage(with: url)
        albumNameLabel.text = album.title
        artistOfAlbumLabel.text = album.artist.name
    }
    
    func succes() {
        albumTableView.reloadData()
    }
    
    func failure(error: Error) {
        print(error.localizedDescription)
    }
}
