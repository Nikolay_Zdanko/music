//
//  AlbumDetailsModel.swift
//  MusicAPP
//
//  Created by Егор Евсеенко on 01.11.2021.
//

import Foundation

struct AlbumDetailsModel: Decodable {
    let data: [AlbumDetails]
}

struct AlbumDetails: Decodable {
    let id: Int
    let title: String
    let link: String
    let duration: Int
    let preview: String
    let artist: ArtistDetails
}

struct ArtistDetails: Decodable {
    let name: String
}
