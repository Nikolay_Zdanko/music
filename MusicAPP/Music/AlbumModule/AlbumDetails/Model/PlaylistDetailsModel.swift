//
//  PlaylistDetailsMoswl.swift
//  MusicAPP
//
//  Created by Егор Евсеенко on 02.11.2021.
//

import Foundation

// MARK: - DreezerAlbums
struct DeezerPlaylist: Decodable {
    let data: [PlaylistDatum]
}

// MARK: - Datum
struct PlaylistDatum: Decodable {
    let id: Int
    let title: String
    let link: String
    let duration: Int
    let preview: String
    let artist: PlaylistArtist
    let album: PlaylistAlbum
}


// MARK: - Album
struct PlaylistAlbum: Codable {
    let id: Int
    let title: String
    let cover: String
    let cover_xl: String
    let tracklist: String
}

// MARK: - PlaylistArtist
struct PlaylistArtist: Codable {
    let id: Int
    let name: String
    let picture_xl: String
    let tracklist: String
}

