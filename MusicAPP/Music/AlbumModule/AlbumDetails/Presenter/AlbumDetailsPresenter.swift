//
//  AlbumDetailsPresenter.swift
//  MusicAPP
//
//  Created by Егор Евсеенко on 01.11.2021.
//

protocol AlbumDetailsViewProtocol: class {
    func setPlaylistInfo(playlist: PlaylistsData)
    func setAlbumInfo(album: AlbumsData)
    func succes()
    func failure(error: Error)
}

protocol AlbumDetailsPresenterProtocol: class {
    init(view: AlbumDetailsViewProtocol, dataFetcherService: AlbumDetailsDataFetcherService, album: AlbumsData)
    init(view: AlbumDetailsViewProtocol, dataFetcherService: AlbumDetailsDataFetcherService, playlist: PlaylistsData)
    var albums: AlbumDetailsModel? { get set }
    var album: AlbumsData? { get set }
    var playlists: DeezerPlaylist? { get set }
    var playlist: PlaylistsData? { get set }
    
    func getAlbums(album: AlbumsData)
    func getPlaylists(playlist: PlaylistsData)
    func numberOfRowsInSection() -> Int
}

class AlbumDetailsPresenter: AlbumDetailsPresenterProtocol {
    
    weak var view: AlbumDetailsViewProtocol?
    var dataFetcherService: AlbumDetailsDataFetcherService
    var albums: AlbumDetailsModel?
    var album: AlbumsData?
    var playlists: DeezerPlaylist?
    var playlist: PlaylistsData?
    
    required init(view: AlbumDetailsViewProtocol, dataFetcherService: AlbumDetailsDataFetcherService, album: AlbumsData) {
        self.view = view
        self.dataFetcherService = dataFetcherService
        getAlbums(album: album)
    }
    
    required init(view: AlbumDetailsViewProtocol, dataFetcherService: AlbumDetailsDataFetcherService, playlist: PlaylistsData) {
        self.view = view
        self.dataFetcherService = dataFetcherService
        getPlaylists(playlist: playlist)
    }
    
    func getAlbums(album: AlbumsData) {
        dataFetcherService.fetchSongs { [weak self] albums in
            guard let self = self,
                  let albums = albums else { return }
            self.albums = albums
            self.album = album
            self.view?.succes()
            self.view?.setAlbumInfo(album: album)
        }
    }
    
    func getPlaylists(playlist: PlaylistsData){
        dataFetcherService.fetchPlaylist { [weak self] playlists in
            guard let self = self else { return }
            self.playlists = playlists
            self.playlist = playlist
            self.view?.succes()
            self.view?.setPlaylistInfo(playlist: playlist)
        }
    }
    
    func numberOfRowsInSection() -> Int {
        guard let numberOfRowsInSection = albums?.data.count else { return playlists?.data.count ?? 0 }
        return numberOfRowsInSection
    }
}

