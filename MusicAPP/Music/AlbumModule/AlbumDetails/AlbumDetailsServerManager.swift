//
//  AlbumDetailsServerManager.swift
//  MusicAPP
//
//  Created by Егор Евсеенко on 01.11.2021.
//

import Foundation

import Foundation

protocol AlbumDetailsDataFetcherServiceProtocol {
    func fetchSongs(completion: @escaping (AlbumDetailsModel?) -> Void)
    func fetchAlbum(completion: @escaping (DeezerModel?) -> Void)
    func fetchPlaylist(completion: @escaping (DeezerPlaylist?) -> Void)
}

class AlbumDetailsDataFetcherService: AlbumDetailsDataFetcherServiceProtocol {
    var networkDataFetcher: NetworkDataFetcher?
    let url: String
    
    init(url: String, networkDataFetcher: NetworkDataFetcher = NetworkDataFetcher()) {
        self.networkDataFetcher = networkDataFetcher
        self.url = url
    }
    
    func fetchSongs(completion: @escaping (AlbumDetailsModel?) -> Void) {
        networkDataFetcher?.fetchGenericJSONData(urlString: url, response: completion)
    }
    
    func fetchAlbum(completion: @escaping (DeezerModel?) -> Void) {
        let urlTopTrack = "https://api.deezer.com/chart"
        networkDataFetcher?.fetchGenericJSONData(urlString: urlTopTrack, response: completion)
    }
    
    func fetchPlaylist(completion: @escaping (DeezerPlaylist?) -> Void) {
        networkDataFetcher?.fetchGenericJSONData(urlString: url, response: completion)
    }
}
