//
//  AlbumPresenter.swift
//  MusicAPP
//
//  Created by Николай on 10.11.21.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

protocol AlbumPresentationLogic: AnyObject {
    func presentAlbums(response: AlbumModels.Music.Response.ResponceType)
}

final class AlbumPresenter {
    // MARK: - Properties
    weak var viewController: AlbumDisplayLogic?
}

// MARK: - AlbumPresentationLogic
extension AlbumPresenter: AlbumPresentationLogic {
    func presentAlbums(response: AlbumModels.Music.Response.ResponceType) {
        switch response {
        case .presentAlbums(data: let data):
            let viewModel = data.map { model -> AlbumViewModel in
                let cell = AlbumViewModel(albumName: model.title,
                                          artistName: model.artist.name,
                                          albumImage: model.cover_xl)
                return cell
            }
            viewController?.displayData(viewModel: .displayAlbum(albumViewModel: viewModel))
        case .presentPlaylist(data: let data):
            let viewModel = data.map { model -> PlaylistViewModel in
                let cell = PlaylistViewModel(title: model.title,
                                             imageX1: model.picture_xl)
                return cell
            }
            viewController?.displayData(viewModel: .displayPlaylist(playlistViewModel: viewModel))
        }
    }
}
