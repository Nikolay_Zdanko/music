//
//  PreferencesCollectionViewCell.swift
//  music
//
//  Created by Николай on 14.10.21.
//

import UIKit

class AlbumCollectionViewCell: UICollectionViewCell {
    
    static let identifier = "AlbumCollectionViewCell"
    
    lazy var albumImageView: UIImageView = {
        var imageViewGenres = UIImageView()
        imageViewGenres.contentMode = .scaleToFill
        imageViewGenres.layer.cornerRadius = AlbumsCollectionConstants.albumImageRadius
        imageViewGenres.translatesAutoresizingMaskIntoConstraints = false
        imageViewGenres.clipsToBounds = true
        return imageViewGenres
    }()
    
    lazy var albumNameLabel: UILabel = {
        var genresNameLabel = UILabel()
        genresNameLabel.font = FontSctuct.bold14
        genresNameLabel.textColor = ColorStruct.forgotLabel
        genresNameLabel.textAlignment = .left
        genresNameLabel.sizeToFit()
        genresNameLabel.translatesAutoresizingMaskIntoConstraints = false
        return genresNameLabel
    }()
    
    lazy var artistOfAlbumLabel: UILabel = {
        var artistOfAlbumLabel = UILabel()
        artistOfAlbumLabel.font = FontSctuct.regular14
        artistOfAlbumLabel.textColor = ColorStruct.artiscOfAlbum
        artistOfAlbumLabel.textAlignment = .left
        artistOfAlbumLabel.sizeToFit()
        artistOfAlbumLabel.translatesAutoresizingMaskIntoConstraints = false
        return artistOfAlbumLabel
    }()
    
    lazy var albumDescription: UIStackView = {
        var albumDescription = UIStackView()
        albumDescription.axis = .vertical
        albumDescription.distribution = .fillEqually
        albumDescription.spacing = 0
        albumDescription.translatesAutoresizingMaskIntoConstraints = false
        albumDescription.addArrangedSubview(albumNameLabel)
        albumDescription.addArrangedSubview(artistOfAlbumLabel)
        return albumDescription
    }()
    
    lazy var cell: UIStackView = {
        var cell = UIStackView()
        cell.axis = .vertical
        cell.spacing = 10
        cell.translatesAutoresizingMaskIntoConstraints = false
        cell.addArrangedSubview(albumImageView)
        cell.addArrangedSubview(albumDescription)
        return cell
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        contentView.addSubview(cell)
        contentView.backgroundColor = ColorStruct.navigationBar

    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let cellSize = (Int(contentView.frame.size.width)) + Int((AlbumsCollectionConstants.albumCollectionInterItemSpacing)) / 2
        
        cell.leadingAnchor.constraint(equalTo: contentView.leadingAnchor).isActive = true
        cell.trailingAnchor.constraint(equalTo: contentView.trailingAnchor).isActive = true
        cell.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
        cell.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
        
        albumDescription.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
        albumDescription.heightAnchor.constraint(equalToConstant: contentView.frame.height * 0.2).isActive = true
    }
}
