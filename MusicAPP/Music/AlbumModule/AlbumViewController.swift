//
//  AlbumViewController.swift
//  MusicAPP
//
//  Created by Николай on 10.11.21.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

protocol AlbumDisplayLogic: AnyObject {
    func displayData(viewModel: AlbumModels.Music.ViewModel.ViewModelType)
}

final class AlbumViewController: UIViewController {
    // MARK: - Properties
    private let interactor: AlbumBusinessLogic
    private let router: AlbumRoutingLogic
    private var albumsViewModel = [AlbumViewModel]()
    private var playlistViewModel = [PlaylistViewModel]()
    private var chart: DeezerModel?
    //    private let searchBar = UISearchBar()
    //    private var discoversearchAllViewModel = [DiscoverSearchViewModel]()
    //    private var timer: Timer?
    
    // MARK: - UI Properties
    static func createAlbumLayout() -> UICollectionViewCompositionalLayout {
        return UICollectionViewCompositionalLayout { (sectionNumber, env) -> NSCollectionLayoutSection? in
            switch sectionNumber {
            case 0:
                let item = NSCollectionLayoutItem(layoutSize: .init(widthDimension: .fractionalWidth(1), heightDimension: .fractionalHeight(1)))
                item.contentInsets = .init(top: PlaylistCollectionConstants.playlistsCollectionTopItemSpacing,
                                           leading: PlaylistCollectionConstants.playlistsCollectionLeftSpacing,
                                           bottom: 0,
                                           trailing: PlaylistCollectionConstants.playlistsCollectionRightSpacing)
                let group = NSCollectionLayoutGroup.horizontal(layoutSize: .init(widthDimension: .fractionalWidth(0.35), heightDimension: .fractionalWidth(0.43)), subitems: [item])
                let section = NSCollectionLayoutSection(group: group)
                section.orthogonalScrollingBehavior = .continuous
                section.boundarySupplementaryItems = [
                    .init(layoutSize: .init(widthDimension: .fractionalWidth(1), heightDimension: .absolute(PlaylistCollectionConstants.heightForHeader)), elementKind: PlaylistCollectionText.playlistsHeader, alignment: .topLeading)
                ]
                return section
            case 1:
                let item = NSCollectionLayoutItem(layoutSize: .init(widthDimension: .fractionalWidth(0.5), heightDimension: .fractionalHeight(1)))
                item.contentInsets = .init(top: AlbumsCollectionConstants.albumCollectionTopItemSpacing,
                                           leading: AlbumsCollectionConstants.albumCollectionLeftSpacing,
                                           bottom:  AlbumsCollectionConstants.albumCollectionBottomItemSpacing,
                                           trailing: AlbumsCollectionConstants.albumCollectionRightSpacing)
                let group = NSCollectionLayoutGroup.horizontal(layoutSize: .init(widthDimension: .fractionalWidth(1), heightDimension: .fractionalWidth(0.65)), subitems: [item])
                let section = NSCollectionLayoutSection(group: group)
                section.boundarySupplementaryItems = [
                    .init(layoutSize: .init(widthDimension: .fractionalWidth(1), heightDimension: .absolute(AlbumsCollectionConstants.heightForHeader)), elementKind: AlbumCollectionText.albumsHeader, alignment: .topLeading)
                ]
                return section
            default: return nil
            }
        }
    }
    
    private lazy var albumCollectionView: UICollectionView = {
        let layout:UICollectionViewCompositionalLayout = AlbumViewController.createAlbumLayout()
        let collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        collectionView.insetsLayoutMarginsFromSafeArea = false
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.showsVerticalScrollIndicator = false
        collectionView.isScrollEnabled = true
        collectionView.backgroundColor = .clear
        collectionView.register(AlbumCollectionViewCell.self,
                                forCellWithReuseIdentifier: AlbumCollectionViewCell.identifier)
        collectionView.register(HeaderForAlbums.self,
                                forSupplementaryViewOfKind: PlaylistCollectionText.playlistsHeader,
                                withReuseIdentifier: AlbumCollectionText.headerId)
        collectionView.register(HeaderForPlaylists.self,
                                forSupplementaryViewOfKind: AlbumCollectionText.albumsHeader,
                                withReuseIdentifier: AlbumCollectionText.headerId)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.allowsMultipleSelection = true
        return collectionView
    }()
    
    private lazy var headerView: UICollectionReusableView = {
        let view = UICollectionReusableView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var playlistsLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = FontSctuct.bold18
        label.textColor = ColorStruct.albumDetailsHeader
        label.textAlignment = .left
        return label
    }()
    
    private lazy var topAlbumsLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = FontSctuct.bold18
        label.textColor = ColorStruct.albumDetailsHeader
        label.textAlignment = .left
        return label
    }()
    
    private lazy var topAlbumsView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    // MARK: - Initialization
    init(interactor: AlbumBusinessLogic, router: AlbumRoutingLogic) {
        self.interactor = interactor
        self.router = router
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented in AlbumViewController")
    }
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = ColorStruct.viewBackground
        configureView()
        startSettings()
    }
    
    // MARK: - Private Methods
    private func startSettings() {
        makeRequest(request: .getAlbums)
        makeRequest(request: .getPlaylist)
    }
    
    private func configureView() {
        setupSubviews()
        setupLayout()
    }
    
    private func setupSubviews() {
        view.addSubview(albumCollectionView)
    }
    
    private func setupLayout() {
        albumCollectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        albumCollectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        albumCollectionView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        albumCollectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }
    
    //     MARK: - Interactor Methods
    func makeRequest(request: AlbumModels.Music.Request.RequestType) {
        switch request {
        case .getAlbums:
            interactor.makeRequest(request: .getAlbums)
        case .getPlaylist:
            interactor.makeRequest(request: .getPlaylist)
        }
    }
}

// MARK: - AlbumDisplayLogic
extension AlbumViewController: AlbumDisplayLogic {
    func displayData(viewModel: AlbumModels.Music.ViewModel.ViewModelType) {
        switch viewModel {
        case .displayAlbum(albumViewModel: let albumViewModel):
            self.albumsViewModel = albumViewModel
            DispatchQueue.main.async {
                self.albumCollectionView.reloadData()
            }
        case .displayPlaylist(playlistViewModel: let playlistViewModel):
            self.playlistViewModel = playlistViewModel
            DispatchQueue.main.async {
                self.albumCollectionView.reloadData()
            }
        }
    }
}
//MARK: - UICollectionViewDataSource
extension AlbumViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind,
                                                                     withReuseIdentifier: AlbumCollectionText.headerId,
                                                                     for: indexPath)
        return header
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return section == 0 ? playlistViewModel.count : albumsViewModel.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: AlbumCollectionViewCell.identifier, for: indexPath) as? AlbumCollectionViewCell else { return UICollectionViewCell() }
        cell.artistOfAlbumLabel.text = ""
        cell.artistOfAlbumLabel.text = ""
        switch indexPath.section {
        case 0:
            let playlist = playlistViewModel[indexPath.row]
            let url = URL(string: playlist.imageX1 ?? "")
            cell.albumImageView.kf.setImage(with: url)
            cell.albumNameLabel.text = playlist.title
            return cell
        case 1:
            let album = albumsViewModel[indexPath.row]
            let url = URL(string: album.albumImage ?? "")
            cell.albumImageView.kf.setImage(with: url)
            cell.albumNameLabel.text = album.albumName
            cell.artistOfAlbumLabel.text = album.artistName
            return cell
        default:
            return UICollectionViewCell()
        }
    }
}

//MARK: - UICollectionViewDelegate
extension AlbumViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(indexPath.section , indexPath.row)
        switch indexPath.section {
        case 0:
            print("")
            //            guard let playlist = presenter.chart?.playlists.data[indexPath.row] else { return }
            //            let playlistDetailsVC = ModuleBuilder.createPlaylistDetails(url: playlist.tracklist, playlist: playlist)
            //            self.navigationController?.pushViewController(playlistDetailsVC, animated: true)
        case 1:
            print("")
            //            guard let album = presenter.chart?.albums.data[indexPath.row] else { return }
            //            let albumDetailsVC = ModuleBuilder.createAlbumDetails(url: album.tracklist, album: album)
            //            self.navigationController?.pushViewController(albumDetailsVC, animated: true)
        default: break
        }
    }
}
