//
//  AlbumAssembly.swift
//  MusicAPP
//
//  Created by Николай on 10.11.21.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

final class AlbumAssembly {
    static func assembly() -> UIViewController {
        let presenter = AlbumPresenter()
        let router = AlbumRouter()
        let worker = AlbumWorker()
        let interactor = AlbumInteractor(presenter: presenter, worker: worker)
        let viewController = AlbumViewController(interactor: interactor, router: router)

        router.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
