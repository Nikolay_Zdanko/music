//
//  AlbumInteractor.swift
//  MusicAPP
//
//  Created by Николай on 10.11.21.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

protocol AlbumBusinessLogic: AnyObject {
   func makeRequest(request: AlbumModels.Music.Request.RequestType)
}

final class AlbumInteractor {
    // MARK: - Properties
    private let presenter: AlbumPresentationLogic
    private let worker: AlbumWorker

    // MARK: - Initialization
    init(presenter: AlbumPresentationLogic, worker: AlbumWorker) {
        self.presenter = presenter
        self.worker = worker
    }
}

// MARK: - AlbumBusinessLogic
extension AlbumInteractor: AlbumBusinessLogic {
    func makeRequest(request: AlbumModels.Music.Request.RequestType) {
        switch request {
        case .getAlbums:
            worker.getData { [weak self] albums in
                guard let self = self else { return }
                guard let data = albums?.albums.data else { return }
                self.presenter.presentAlbums(response: .presentAlbums(data: data))
            }
        case .getPlaylist:
            worker.getData { [weak self] playlist in
                guard let self = self else { return }
                guard let data = playlist?.playlists.data else { return }
                self.presenter.presentAlbums(response: .presentPlaylist(data: data))
            }
        }
    }
}
