//
//  HeaderForAlbums.swift
//  MusicAPP
//
//  Created by Егор Евсеенко on 08.11.2021.
//

import UIKit

class HeaderForAlbums: UICollectionReusableView {
    lazy var label: UILabel = {
        let label = UILabel()
        label.textColor = .green
        label.text = AlbumDetailsText.playlists
        label.font = FontSctuct.bold18
        label.textColor = ColorStruct.albumDetailsHeader
        label.textAlignment = .left
        return label
    }()
    
    override init(frame: CGRect){
        super.init(frame: frame)
        addSubview(label)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        label.frame = CGRect(x: AlbumsCollectionConstants.albumCollectionLeftSpacing, y: 0, width: bounds.width, height: bounds.height)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


