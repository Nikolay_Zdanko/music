//
//  AlbumRouter.swift
//  MusicAPP
//
//  Created by Николай on 10.11.21.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

protocol AlbumRoutingLogic: AnyObject {
    func back()
}

final class AlbumRouter: AlbumRoutingLogic {
    // MARK: - Properties
    weak var viewController: AlbumViewController?
    
    func back() {
        viewController?.navigationController?.popViewController(animated: true)
    }
}
