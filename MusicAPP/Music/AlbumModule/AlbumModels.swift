//
//  AlbumModels.swift
//  MusicAPP
//
//  Created by Николай on 10.11.21.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

enum AlbumModels {
    enum Music {
        struct Request {
            enum RequestType {
                case getAlbums
                case getPlaylist
            }
        }
        struct Response {
            enum ResponceType {
                case presentAlbums(data: [AlbumsData])
                case presentPlaylist(data: [PlaylistsData])
            }
        }
        struct ViewModel {
            enum ViewModelType {
                case displayAlbum(albumViewModel: [AlbumViewModel])
                case displayPlaylist(playlistViewModel: [PlaylistViewModel])
            }
        }
    }
}

struct AlbumViewModel {
        var albumName: String?
        var artistName: String?
        var albumImage: String?
}

struct PlaylistViewModel {
    var title: String
    var imageX1: String?
}
