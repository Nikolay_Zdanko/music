//
//  AlbumWorker.swift
//  MusicAPP
//
//  Created by Николай on 10.11.21.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

final class AlbumWorker {
    var dataFetcherService =  DataFetcherService()
   
   func getData(completion: @escaping (DeezerModel?) -> Void) {
       dataFetcherService.fetchData(completion: { [weak self] tracks in
           guard let self = self else { return }
           completion(tracks)
       })
   }
}
