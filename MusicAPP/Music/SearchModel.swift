//
//  SearchModel.swift
//  MusicAPP
//
//  Created by Николай on 10.11.21.
//

import Foundation

// MARK: - Welcome
struct SearchModel: Decodable {
    let data: [Datum]
    let total: Int
    let next: String
}

// MARK: - Datum
struct Datum: Decodable {
    let readable: Bool
    let title, titleShort: String
    let link: String
    let duration, rank: Int
    let explicitLyrics: Bool
    let explicitContentLyrics, explicitContentCover: Int
    let preview: String
    let md5Image: String
    let artist: ArtistSearch
    let album: Album

    enum CodingKeys: String, CodingKey {
        case readable, title
        case titleShort = "title_short"
        case link, duration, rank
        case explicitLyrics = "explicit_lyrics"
        case explicitContentLyrics = "explicit_content_lyrics"
        case explicitContentCover = "explicit_content_cover"
        case preview
        case md5Image = "md5_image"
        case artist, album
    }
}

// MARK: - Album
struct Album: Decodable {
    let id: Int
    let title: String
    let cover: String
    let coverSmall, coverMedium, coverBig, coverXl: String
    let md5Image: String
    let tracklist: String
    let type: AlbumType

    enum CodingKeys: String, CodingKey {
        case id, title, cover
        case coverSmall = "cover_small"
        case coverMedium = "cover_medium"
        case coverBig = "cover_big"
        case coverXl = "cover_xl"
        case md5Image = "md5_image"
        case tracklist, type
    }
}

enum AlbumType: String, Codable {
    case album = "album"
}

// MARK: - Artist
struct ArtistSearch: Decodable {
    let link, picture: String
    let pictureSmall, pictureMedium, pictureBig, pictureXl: String
    let tracklist: String

    enum CodingKeys: String, CodingKey {
        case link, picture
        case pictureSmall = "picture_small"
        case pictureMedium = "picture_medium"
        case pictureBig = "picture_big"
        case pictureXl = "picture_xl"
        case tracklist
    }
}
