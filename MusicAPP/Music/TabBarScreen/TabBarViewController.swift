//
//  TabBarViewController.swift
//  mvp
//
//  Created by Николай on 27.10.21.
//

import UIKit

class TabBarViewController: UITabBarController {
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTabBar()
        setupControllers()
    }
    
    func setupControllers() {
        viewControllers = [
            generateViewController(rootViewController: DiscoverAssembly.assembly(), image: ImageNameStruct.search ?? UIImage(), title: TextStruct.discoverTitle),
            generateViewController(rootViewController: AlbumAssembly.assembly(), image: ImageNameStruct.album ?? UIImage(), title: TextStruct.albumTitle),
            generateViewController(rootViewController: ArtistViewController(), image: ImageNameStruct.atrist ?? UIImage(), title: TextStruct.artistTitle),
            generateViewController(rootViewController: FavouritesAssembly.assembly(), image: ImageNameStruct.favourites ?? UIImage(), title: TextStruct.favourites),
            generateViewController(rootViewController: ProfileViewController(), image: ImageNameStruct.profile ?? UIImage(), title: TextStruct.profile)
        ]
    }
    private func generateViewController(rootViewController:UIViewController, image: UIImage, title: String) -> UIViewController {
        let navigationVC = UINavigationController(rootViewController: rootViewController)
        let barBackgroundColor = ColorStruct.viewBackground
        let appearance = UINavigationBarAppearance()
        appearance.configureWithDefaultBackground()
        appearance.backgroundColor = barBackgroundColor
        appearance.titleTextAttributes = [NSAttributedString.Key.foregroundColor: ColorStruct.forgotLabel,
                                          NSAttributedString.Key.font: FontSctuct.semibold17 as Any 
                                         ]
        UINavigationBar.appearance().standardAppearance = appearance
        UINavigationBar.appearance().scrollEdgeAppearance = appearance
        navigationVC.tabBarItem.image = image
        navigationVC.tabBarItem.title = title
        navigationVC.tabBarItem.setTitleTextAttributes([NSAttributedString.Key.font: FontSctuct.regular12 ?? UIFont()] as [NSAttributedString.Key : Any], for: .normal)
        rootViewController.navigationItem.title = title
        return navigationVC
    }


}
