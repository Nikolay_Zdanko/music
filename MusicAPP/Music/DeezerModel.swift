//
//  DeezerModel.swift
//  mvp
//
//  Created by Николай on 25.10.21.
//

import Foundation

// MARK: - DeezerModel
struct DeezerModel: Decodable {
    let tracks: Tracks
    let albums: Albums
    let playlists: Playlists
}
// MARK: - Tracks
struct Tracks: Decodable {
    let data: [DataTrack]
}

// MARK: - Albums
struct Albums: Decodable {
    let data: [AlbumsData]
    let total: Int
}

// MARK: - AlbumsDatum
struct AlbumsData: Decodable {
    let id: Int
    let title: String
    let link: String
    let cover_xl: String?
    let tracklist: String
    let artist: ArtistElement
}

// MARK: - ArtistElement
struct ArtistElement: Decodable {
    let name: String
    let link: String
    let picture: String
    let tracklist: String
}

// MARK: - Data
struct DataTrack: Decodable {
    let title: String
    let duration: Int
    let artist: Artist
}
//MARK: - Artist
struct Artist: Decodable {
    let name: String
    let picture: String
}

//MARK: - Playlist
struct Playlists: Decodable {
    let data: [PlaylistsData]
    let total: Int
}

// MARK: - PlaylistsDatum
struct PlaylistsData: Decodable{
    let id: Int
    let title: String
    let link: String
    let picture: String?
    let picture_big: String?
    let picture_xl: String?
    let tracklist: String
}

