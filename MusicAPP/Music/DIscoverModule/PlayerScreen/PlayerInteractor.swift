//
//  PlayerInteractor.swift
//  MusicAPP
//
//  Created by Николай on 8.11.21.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

protocol PlayerBusinessLogic: AnyObject {
    func makeRequest(request: Player.Music.Request.RequestType)
}

protocol PlayerDataStoreProtocol: AnyObject {
    var model: DiscoverTracksViewModel? { get set }
}

final class PlayerInteractor: PlayerDataStoreProtocol {
    
    // MARK: - Properties
    private let presenter: PlayerPresentationLogic
    private let worker: PlayerWorker
    var model: DiscoverTracksViewModel?

    // MARK: - Initialization
    init(presenter: PlayerPresentationLogic, worker: PlayerWorker) {
        self.presenter = presenter
        self.worker = worker
    }
}

// MARK: - PlayerBusinessLogic
extension PlayerInteractor: PlayerBusinessLogic {
    func makeRequest(request: Player.Music.Request.RequestType) {
        switch request {
        case .getTrack:
            presenter.presentTrack(response: .presentTrack(response: model))
        }
    }
}
