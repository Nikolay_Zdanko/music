//
//  PlayerAssembly.swift
//  MusicAPP
//
//  Created by Николай on 8.11.21.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

final class PlayerAssembly {
    static func assembly() -> UIViewController {
        let presenter = PlayerPresenter()
        let router = PlayerRouter()
        let worker = PlayerWorker()
        let interactor = PlayerInteractor(presenter: presenter, worker: worker)
        let viewController = PlayerViewController(interactor: interactor, router: router)

        router.viewController = viewController
        router.dataStore = interactor
        presenter.viewController = viewController

        return viewController
    }
}
