//
//  PlayerRouter.swift
//  MusicAPP
//
//  Created by Николай on 8.11.21.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

protocol PlayerRoutingLogic: AnyObject {
    func back()
}

protocol PlayerDataPassingProtocol {
    var dataStore: PlayerDataStoreProtocol? { get }
}

final class PlayerRouter: PlayerDataPassingProtocol {
    // MARK: - Properties
    weak var viewController: PlayerViewController?
    weak var dataStore: PlayerDataStoreProtocol?

}

extension PlayerRouter: PlayerRoutingLogic {
    func back() {
        
    }
}
