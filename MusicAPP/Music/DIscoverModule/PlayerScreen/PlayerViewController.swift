//
//  PlayerViewController.swift
//  MusicAPP
//
//  Created by Николай on 8.11.21.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

protocol PlayerDisplayLogic: AnyObject {
    func displayData(viewModel: Player.Music.ViewModel.ViewModelType)
}

final class PlayerViewController: UIViewController {
    // MARK: - Nested Types
    private enum Constants {
    }

    // MARK: - Properties
    private let interactor: (PlayerBusinessLogic & PlayerDataStoreProtocol)
    private(set) var router: (PlayerRoutingLogic & PlayerDataPassingProtocol)

    // MARK: - UI Properties
    private lazy var trackNameLabel: UILabel = {
       var label = UILabel()
        label.textColor = .black
        label.translatesAutoresizingMaskIntoConstraints = false
       return label
    }()
    private lazy var artistNameLabel: UILabel = {
       var label = UILabel()
        label.textColor = .black
        label.translatesAutoresizingMaskIntoConstraints = false

       return label
    }()

    // MARK: - Initialization
    init(interactor: (PlayerBusinessLogic & PlayerDataStoreProtocol), router: (PlayerRoutingLogic & PlayerDataPassingProtocol)) {
        self.interactor = interactor
        self.router = router
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented in PlayerViewController")
    }

    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        configureView()
        startSettings()
    }

    // MARK: - Private Methods
    private func startSettings() {
        makeRequest(request: .getTrack)
    }

    private func configureView() {
        setupSubviews()
        setupLayout()
    }

    private func setupSubviews() {
        view.addSubview(trackNameLabel)
        view.addSubview(artistNameLabel)
    }

    private func setupLayout() {
        trackNameLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        trackNameLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
        trackNameLabel.widthAnchor.constraint(equalToConstant: 30).isActive = true
        artistNameLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        artistNameLabel.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        artistNameLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
        artistNameLabel.widthAnchor.constraint(equalToConstant: 30).isActive = true
    }

    // MARK: - Interactor Methods
    func makeRequest(request: Player.Music.Request.RequestType) {
        switch request {
        case .getTrack:
            interactor.makeRequest(request: .getTrack)
        }
    }
}

// MARK: - PlayerDisplayLogic
extension PlayerViewController: PlayerDisplayLogic {
    func displayData(viewModel: Player.Music.ViewModel.ViewModelType) {
        switch viewModel {
        case .displayTrack(playerViewModel: let playerViewModel):
            trackNameLabel.text = playerViewModel.trackName
            artistNameLabel.text = playerViewModel.artistName
            print(artistNameLabel.text ?? "")
        }
    }
}
