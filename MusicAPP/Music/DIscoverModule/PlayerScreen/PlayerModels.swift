//
//  PlayerModels.swift
//  MusicAPP
//
//  Created by Николай on 8.11.21.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

enum Player {
    enum Music {
        struct Request {
            enum RequestType {
                case getTrack
            }
        }
        struct Response {
            enum ResponseType {
                case presentTrack(response: DiscoverTracksViewModel?)
            }
        }
        struct ViewModel {
            enum ViewModelType {
                case displayTrack(playerViewModel: DiscoverTracksViewModel)
            }
        }
    }
}
