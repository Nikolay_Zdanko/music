//
//  PlayerPresenter.swift
//  MusicAPP
//
//  Created by Николай on 8.11.21.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

protocol PlayerPresentationLogic: AnyObject {
    func presentTrack(response: Player.Music.Response.ResponseType)
}

final class PlayerPresenter {
    // MARK: - Properties
    weak var viewController: PlayerDisplayLogic?
}

// MARK: - PlayerPresentationLogic
extension PlayerPresenter: PlayerPresentationLogic {
    func presentTrack(response: Player.Music.Response.ResponseType) {
        switch response {
        case .presentTrack(response: let response):
            guard let modelResponse = response else { return }
            viewController?.displayData(viewModel: .displayTrack(playerViewModel: modelResponse))
        }
    }
}
