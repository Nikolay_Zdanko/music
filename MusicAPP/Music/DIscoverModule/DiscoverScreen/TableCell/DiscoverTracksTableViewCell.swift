//
//  DiscoverTracksTableViewCell.swift
//  mvp
//
//  Created by Николай on 25.10.21.
//

import UIKit

class DiscoverTracksTableViewCell: UITableViewCell {
    // MARK: - Variable
    static let identifier = "DiscoverTracksTableViewCell"
    lazy var trackImage: UIImageView = {
        var imageView = UIImageView()
        imageView.layer.cornerRadius = DimensionsStruct.cornerRadius8
        imageView.clipsToBounds = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    lazy var trackNameLabel: UILabel = {
        var label = UILabel()
        label.textAlignment = .left
        label.textColor = ColorStruct.forgotLabel
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    lazy var artistNameLabel: UILabel = {
        var label = UILabel()
        label.textAlignment = .left
        label.textColor = ColorStruct.textFieldPlaceholder
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    lazy var stackViewVertical: UIStackView = {
        var stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .equalSpacing
        stackView.spacing = DimensionsStruct.spacingStackVertical
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.addArrangedSubview(trackNameLabel)
        stackView.addArrangedSubview(artistNameLabel)
        return stackView
    }()
    lazy var timeSongLabel: UILabel = {
        var label = UILabel()
        label.textColor = ColorStruct.textFieldPlaceholder
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    lazy var buttonSheet: UIButton = {
        var button = UIButton()
        button.setImage(ImageNameStruct.buttonTable, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    lazy var stackViewHorizontal: UIStackView = {
        var stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.distribution = .equalSpacing
        stackView.spacing = 16
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.addArrangedSubview(timeSongLabel)
        stackView.addArrangedSubview(buttonSheet)
        return stackView
    }()
    // MARK: - Lifecycle function
    override func prepareForReuse() {
        super.prepareForReuse()
        trackImage.image = nil
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        contentView.addSubview(trackImage)
        contentView.addSubview(stackViewHorizontal)
        contentView.addSubview(stackViewVertical)
        contentView.backgroundColor = ColorStruct.viewBackground
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        NSLayoutConstraint.activate([
            trackImage.widthAnchor.constraint(equalToConstant: 41),
            trackImage.heightAnchor.constraint(equalToConstant: 41),
            trackImage.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            trackImage.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 24),
            stackViewHorizontal.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 18),
            stackViewHorizontal.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -19),
            stackViewHorizontal.leadingAnchor.constraint(equalTo: stackViewVertical.trailingAnchor, constant: 15),
            stackViewHorizontal.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -24),

            stackViewVertical.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 8),
            stackViewVertical.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -8),
            stackViewVertical.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 90),
            stackViewVertical.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -115),
        ])
    }
}
//MARK: - ConfigurableCell
extension DiscoverTracksTableViewCell: ConfigurableCell {
    typealias DataType = DiscoverTracksViewModel
    func configure(with object: DiscoverTracksViewModel) {
        trackNameLabel.text = object.trackName
        artistNameLabel.text = object.artistName
        let url = URL(string: object.trackImage ?? "")
        trackImage.kf.setImage(with: url)
        timeSongLabel.text = object.trackDuraction
    }
}
