//
//  DiscoverAlbumsTableViewCell.swift
//  MusicAPP
//
//  Created by Николай on 10.11.21.
//

import UIKit
import Kingfisher

protocol ConfigurableCell {
    associatedtype DataType
    func configure(with object: DataType)
}

class DiscoverAlbumsTableViewCell: UITableViewCell {
    
    // MARK: - Variable
    static let identifier = "DiscoverAlbumsTableViewCell"
    lazy var albumImage: UIImageView = {
        var imageView = UIImageView()
        imageView.layer.cornerRadius = DimensionsStruct.cornerRadius8
        imageView.clipsToBounds = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    lazy var albumNameLabel: UILabel = {
        var label = UILabel()
        label.textAlignment = .left
        label.textColor = ColorStruct.forgotLabel
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    lazy var artistNameLabel: UILabel = {
        var label = UILabel()
        label.textAlignment = .left
        label.textColor = ColorStruct.textFieldPlaceholder
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    lazy var stackViewVertical: UIStackView = {
        var stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .equalSpacing
        stackView.spacing = DimensionsStruct.spacingStackVertical
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.addArrangedSubview(albumNameLabel)
        stackView.addArrangedSubview(artistNameLabel)
        return stackView
    }()
    lazy var buttonSheet: UIButton = {
        var button = UIButton()
        button.setImage(ImageNameStruct.buttonTable, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    // MARK: - Lifecycle function
    
    override func prepareForReuse() {
        super.prepareForReuse()
        albumImage.image = nil
    }
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        contentView.addSubview(albumImage)
        contentView.addSubview(stackViewVertical)
        contentView.addSubview(buttonSheet)
        contentView.backgroundColor = ColorStruct.viewBackground
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        NSLayoutConstraint.activate([
            albumImage.widthAnchor.constraint(equalToConstant: 41),
            albumImage.heightAnchor.constraint(equalToConstant: 41),
            albumImage.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            albumImage.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 24),
            buttonSheet.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 18),
            buttonSheet.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -19),
            buttonSheet.leadingAnchor.constraint(equalTo: stackViewVertical.trailingAnchor, constant: 15),
            buttonSheet.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -24),
            
            stackViewVertical.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 8),
            stackViewVertical.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -8),
            stackViewVertical.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 90),
            stackViewVertical.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -115),
        ])
        
    }
    
}
//MARK: - ConfigurableCell
extension DiscoverAlbumsTableViewCell: ConfigurableCell {
    typealias DataType = DiscoverAlbumsViewModel
    func configure(with object: DiscoverAlbumsViewModel) {
        albumNameLabel.text = object.artistName
        artistNameLabel.text = object.artistName
        let url = URL(string: object.albumImage ?? "")
        albumImage.kf.setImage(with: url)
    }
}
