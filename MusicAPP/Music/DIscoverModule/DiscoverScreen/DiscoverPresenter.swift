//
//  DiscoverPresenter.swift
//  MusicAPP
//
//  Created by Николай on 8.11.21.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.
//
import Foundation

protocol DiscoverPresentationLogic: AnyObject {
    func presentData(response: Discover.Music.Response.ResponseType)
}

final class DiscoverPresenter {
    // MARK: - Properties
    weak var viewController: DiscoverDisplayLogic?
}

// MARK: - DiscoverPresentationLogic
extension DiscoverPresenter: DiscoverPresentationLogic {
    func presentData(response: Discover.Music.Response.ResponseType) {
        switch response {
        case .presentTracks(let trackResult):
            let viewModel = trackResult.tracks.data.map { model -> DiscoverTracksViewModel in
                let cell = DiscoverTracksViewModel(trackName: model.title,
                                                   artistName: model.artist.name,
                                                   trackImage: model.artist.picture,
                                                   trackDuraction: convertSecondsToHrMinuteSec(seconds: model.duration))
                return cell
            }
            viewController?.displayData(viewModel: .displayTracks(discoverViewModel: viewModel))
        case .presentAlbums(data: let albumsResult):
            let viewModel = albumsResult.albums.data.map { model -> DiscoverAlbumsViewModel in
                let cell = DiscoverAlbumsViewModel(albumName: model.title,
                                                   artistName: model.artist.name,
                                                   albumImage: model.cover_xl)
                return cell
            }
            viewController?.displayData(viewModel: .displayAlbum(discoverViewModel: viewModel))
        }
    }
    private func convertSecondsToHrMinuteSec(seconds:Int) -> String{
        let formatter = DateComponentsFormatter()
        formatter.allowedUnits = [.hour, .minute, .second]
        formatter.unitsStyle = .positional
        let formattedString = formatter.string(from:TimeInterval(seconds))!
        return formattedString
    }
}
