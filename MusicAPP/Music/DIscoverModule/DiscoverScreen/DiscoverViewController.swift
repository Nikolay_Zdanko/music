//
//  DiscoverViewController.swift
//  MusicAPP
//
//  Created by Николай on 8.11.21.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit
import Kingfisher

protocol DiscoverDisplayLogic: AnyObject {
    func displayData(viewModel: Discover.Music.ViewModel.ViewModelType)
}

final class DiscoverViewController: UIViewController {
    // MARK: - Properties
//  private var interactor: (DiscoverBusinessLogic & DiscoverDataStore)
//  private var router: (DiscoverRoutingLogic & DiscoverDataPassing)
    private var interactor: DiscoverBusinessLogic
    private var router: DiscoverRoutingLogic
    private var discoverTracksViewModel = [DiscoverTracksViewModel]()
    private var discoverAlbumsViewModel = [DiscoverAlbumsViewModel]()
    private let searchBar = UISearchBar()
    private var timer: Timer?
    private var numberOfSections = ["Tracks", "Artists", "Albums"]
    
    // MARK: - UI Properties
    private lazy var segmentControl: UISegmentedControl = {
        let items = ["Recommended","Popular"]
        var control = UISegmentedControl(items: items)
        control.selectedSegmentIndex = 0
        control.layer.cornerRadius = DimensionsStruct.cornerRadius8
        control.layer.masksToBounds = true
        control.tintColor = ColorStruct.forgotLabel
        control.backgroundColor = ColorStruct.textFieldColor
        control.translatesAutoresizingMaskIntoConstraints = false
        control.addTarget(self, action: #selector(handleSegmentControllerValueChanged), for: .valueChanged)
        return control
    }()
    
    private lazy var tableView: UITableView = {
        var tableView = UITableView()
        tableView = UITableView()
        tableView.register(DiscoverTracksTableViewCell.self, forCellReuseIdentifier: DiscoverTracksTableViewCell.identifier)
        tableView.register(DiscoverAlbumsTableViewCell.self, forCellReuseIdentifier: DiscoverAlbumsTableViewCell.identifier)
        tableView.separatorStyle = .singleLine
        tableView.separatorColor = ColorStruct.textFieldPlaceholder
        tableView.backgroundColor = .clear
        tableView.separatorInset = DimensionsStruct.separatorTableView
        tableView.showsVerticalScrollIndicator = false
        tableView.tableFooterView = UIView()
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 44
        tableView.delegate = self
        tableView.dataSource = self
        tableView.translatesAutoresizingMaskIntoConstraints = false
        return tableView
    }()
    
    // MARK: - Initialization
    init(interactor: (DiscoverBusinessLogic), router: (DiscoverRoutingLogic)) {
        self.interactor = interactor
        self.router = router
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented in DiscoverViewController")
    }
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        startSettings()
    }
    
    //     MARK: - Private Methods
    private func startSettings() {
        makeRequest(request: .getTracks)
    }
    
    private func configureView() {
        setupSubviews()
        setupLayout()
        getItem()
        view.backgroundColor = ColorStruct.viewBackground
    }
    
    private func setupSubviews() {
        view.addSubview(tableView)
        view.addSubview(segmentControl)
    }
    
    private func setupLayout() {
        NSLayoutConstraint.activate([
            segmentControl.topAnchor.constraint(equalTo: view.topAnchor, constant: 112),
            segmentControl.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 24),
            segmentControl.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -24),
            segmentControl.heightAnchor.constraint(equalToConstant: 28),
            tableView.topAnchor.constraint(equalTo: segmentControl.topAnchor, constant: 50),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
    }
    
    private func getItem() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .search,
                                                            target: self,
                                                            action: #selector(searchAction))
        navigationItem.rightBarButtonItem?.tintColor = ColorStruct.forgotLabel
    }
    // MARK: - Interactor Methods
    func makeRequest(request: Discover.Music.Request.RequestType) {
        switch request {
        case .getTracks:
            interactor.makeRequest(request: .getTracks)
        case .getAlbums:
            interactor.makeRequest(request: .getAlbums)
        }
    }
    // MARK: - Actions
    @objc
    private func handleSegmentControllerValueChanged(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0:
            makeRequest(request: .getTracks)
        case 1:
            makeRequest(request: .getAlbums)
        default:
            break
        }
    }
    
    @objc
    private func searchAction(_ sender: UIBarButtonItem) {
        router.showSearchScreen()
    }
}
// MARK: - DiscoverDisplayLogic
extension DiscoverViewController: DiscoverDisplayLogic {
    func displayData(viewModel: Discover.Music.ViewModel.ViewModelType) {
        switch viewModel {
        case .displayTracks(discoverViewModel: let discoverTracksViewModel):
            self.discoverTracksViewModel = discoverTracksViewModel
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        case .displayAlbum(discoverViewModel: let discoverAlbumsViewModel):
            self.discoverAlbumsViewModel = discoverAlbumsViewModel
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
}
//MARK: - UITableViewDataSource
extension DiscoverViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch segmentControl.selectedSegmentIndex {
        case 0:
            return discoverTracksViewModel.count
        case 1:
            return  discoverAlbumsViewModel.count
        default:
            break
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch segmentControl.selectedSegmentIndex {
        case 0:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: DiscoverTracksTableViewCell.identifier) as? DiscoverTracksTableViewCell else { return UITableViewCell()}
            let track = discoverTracksViewModel[indexPath.row]
            cell.configure(with: track)
            return cell
        case 1:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: DiscoverAlbumsTableViewCell.identifier) as? DiscoverAlbumsTableViewCell else { return UITableViewCell()}
            let album = discoverAlbumsViewModel[indexPath.row]
            cell.configure(with: album)
            return cell
        default:
            break
        }
        return UITableViewCell()
    }
}
//MARK: - UITableViewDelegate
extension DiscoverViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let track = discoverTracksViewModel[indexPath.row]
        router.showPlayer(dataTrack: track)
    }
}
