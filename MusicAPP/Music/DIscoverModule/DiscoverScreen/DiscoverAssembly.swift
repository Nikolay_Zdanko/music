//
//  DiscoverAssembly.swift
//  MusicAPP
//
//  Created by Николай on 8.11.21.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

final class DiscoverAssembly {
    static func assembly() -> UIViewController {
        let presenter = DiscoverPresenter()
        let router = DiscoverRouter()
        let worker = DiscoverWorker()
        let interactor = DiscoverInteractor(presenter: presenter, worker: worker)
        let viewController = DiscoverViewController(interactor: interactor, router: router)

        router.viewController = viewController
//        router.dataStore = interactor
        presenter.viewController = viewController
        
        return viewController
    }
}
