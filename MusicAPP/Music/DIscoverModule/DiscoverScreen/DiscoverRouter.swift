//
//  DiscoverRouter.swift
//  MusicAPP
//
//  Created by Николай on 8.11.21.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

protocol DiscoverRoutingLogic: AnyObject {
    func showPlayer(dataTrack: DiscoverTracksViewModel)
    func showSearchScreen()
}

final class DiscoverRouter {
    // MARK: - Properties
    weak var viewController: UIViewController?
}
//MARK: - DiscoverRoutingLogic
extension DiscoverRouter: DiscoverRoutingLogic {
    func showPlayer(dataTrack: DiscoverTracksViewModel) {
        guard let playerViewController = PlayerAssembly.assembly() as? PlayerViewController else { return }
        playerViewController.router.dataStore?.model = dataTrack
        playerViewController.modalPresentationStyle = .formSheet
        viewController?.present(playerViewController, animated: true, completion: nil)
    }
    
    func showSearchScreen() {
        guard let searchViewController = SearchAssembly.assembly() as? SearchViewController else { return }
        searchViewController.modalPresentationStyle = .formSheet
        viewController?.present(searchViewController, animated: true, completion: nil)
    }
}
