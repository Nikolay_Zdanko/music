//
//  DiscoverModels.swift
//  MusicAPP
//
//  Created by Николай on 8.11.21.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

enum Discover {
    enum Music {
        struct Request {
            enum RequestType {
                case getTracks
                case getAlbums
            }
        }
        struct Response {
            enum ResponseType {
                case presentTracks(data: DeezerModel)
                case presentAlbums(data: DeezerModel)
            }
        }
        struct ViewModel {
            enum ViewModelType {
                case displayTracks(discoverViewModel: [DiscoverTracksViewModel])
                case displayAlbum(discoverViewModel: [DiscoverAlbumsViewModel])
            }
        }
    }
}

struct DiscoverTracksViewModel {
        var trackName: String?
        var artistName: String?
        var trackImage: String?
        var trackDuraction: String?
}

struct DiscoverAlbumsViewModel {
        var albumName: String?
        var artistName: String?
        var albumImage: String?
}
