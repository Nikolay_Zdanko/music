//
//  DiscoverInteractor.swift
//  MusicAPP
//
//  Created by Николай on 8.11.21.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

protocol DiscoverBusinessLogic: AnyObject {
    func makeRequest(request: Discover.Music.Request.RequestType)
}

final class DiscoverInteractor {
    // MARK: - Properties
    private let presenter: DiscoverPresentationLogic
    private let worker: DiscoverWorker
    
    var model: DiscoverTracksViewModel?

    // MARK: - Initialization
    init(presenter: DiscoverPresentationLogic, worker: DiscoverWorker) {
        self.presenter = presenter
        self.worker = worker
    }
}

// MARK: - DiscoverBusinessLogic
extension DiscoverInteractor: DiscoverBusinessLogic {
    func makeRequest(request: Discover.Music.Request.RequestType) {
        switch request {
        case .getTracks:
            worker.getData { [weak self] tracks in
                guard let self = self else { return }
                guard let data = tracks else { return }
                self.presenter.presentData(response: .presentTracks(data: data))
            }
        case .getAlbums:
            worker.getData { [weak self] albums in
                guard let self = self else { return }
                guard let data = albums else { return }
                self.presenter.presentData(response: .presentAlbums(data: data))
            }
        }
    }
}
