//
//  SearchInteractor.swift
//  MusicAPP
//
//  Created by Николай on 11.11.21.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

protocol SearchBusinessLogic: AnyObject {
    func makeRequest(request: Search.All.Request.RequestType)
}

final class SearchInteractor {
    // MARK: - Properties
    private let presenter: SearchPresentationLogic
    private let worker: SearchWorker

    // MARK: - Initialization
    init(presenter: SearchPresentationLogic, worker: SearchWorker) {
        self.presenter = presenter
        self.worker = worker
    }
}

// MARK: - SearchBusinessLogic
extension SearchInteractor: SearchBusinessLogic {
    func makeRequest(request: Search.All.Request.RequestType) {
        switch request {
        case .getAll(searchTerm: let searchTerm):
            worker.getSearchData(searchText: searchTerm) { [weak self] searchResponse in
                guard let self = self else { return }
                self.presenter.presentData(response: .presentAll(searchResponse: searchResponse))
            }
        }
    }
}
