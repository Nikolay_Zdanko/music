//
//  SearchAssembly.swift
//  MusicAPP
//
//  Created by Николай on 11.11.21.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

final class SearchAssembly {
    static func assembly() -> UIViewController {
        let presenter = SearchPresenter()
        let router = SearchRouter()
        let worker = SearchWorker()
        let interactor = SearchInteractor(presenter: presenter, worker: worker)
        let viewController = SearchViewController(interactor: interactor, router: router)

        router.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
