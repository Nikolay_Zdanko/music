//
//  SearchRouter.swift
//  MusicAPP
//
//  Created by Николай on 11.11.21.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

protocol SearchRoutingLogic: AnyObject {
    func back()
}

final class SearchRouter: SearchRoutingLogic {
    // MARK: - Properties
    weak var viewController: SearchViewController?
    
    func back() {
        viewController?.navigationController?.popViewController(animated: true)
    }
}
