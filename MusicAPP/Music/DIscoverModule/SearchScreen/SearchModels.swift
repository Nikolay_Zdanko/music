//
//  SearchModels.swift
//  MusicAPP
//
//  Created by Николай on 11.11.21.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

enum Search {
    enum All {
        struct Request {
            enum RequestType {
                case getAll(searchTerm: String)
            }
        }
        struct Response {
            enum ResponseType {
                case presentAll(searchResponse: SearchModel?)
            }
        }
        struct ViewModel {
            enum ViewModelType {
                case displayAll(searchViewModel: [SearchViewModel])
            }
        }
    }
}

struct SearchViewModel {
    var trackName: String
    var trackImage: String?
    var artistName: String
    var albumTitle: String
    var albumImage: String?
}
