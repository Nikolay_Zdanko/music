//
//  SearchWorker.swift
//  MusicAPP
//
//  Created by Николай on 11.11.21.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

final class SearchWorker {
    var dataFetcherService =  DataFetcherService()
    
    func getSearchData(searchText: String, completion: @escaping (SearchModel?) -> Void) {
        dataFetcherService.searchData(searchText: searchText) { [weak self] data in
            guard let self = self else { return }
            completion(data)
        }
    }
}
