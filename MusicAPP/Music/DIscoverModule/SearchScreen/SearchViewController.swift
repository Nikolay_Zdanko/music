//
//  SearchViewController.swift
//  MusicAPP
//
//  Created by Николай on 11.11.21.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

protocol SearchDisplayLogic: AnyObject {
    func displaySearch(viewModel: Search.All.ViewModel.ViewModelType)
}

final class SearchViewController: UIViewController {
    // MARK: - Properties
    private let interactor: SearchBusinessLogic
    private let router: SearchRoutingLogic
    private var searchAllViewModel = [SearchViewModel]()
    private var timer: Timer?
    private var numberOfSections = ["Tracks", "Artists", "Albums"]
    // MARK: - UI Properties
    private lazy var searchBar: UISearchBar = {
        var searchBar = UISearchBar()
        searchBar.barTintColor = ColorStruct.viewBackground
        searchBar.tintColor = ColorStruct.forgotLabel
        searchBar.searchTextField.textColor = ColorStruct.forgotLabel
        searchBar.searchTextField.placeholder = "Enter album, track or artist"
        searchBar.searchTextField.backgroundColor = ColorStruct.textFieldColor
        searchBar.searchTextField.layer.masksToBounds = true
        searchBar.searchTextField.layer.cornerRadius = 17
        searchBar.searchTextField.layer.borderWidth = CGFloat(0.5)
        searchBar.searchTextField.layer.borderColor = ColorStruct.textFieldPlaceholder.cgColor
        searchBar.searchTextField.leftView(ImageNameStruct.searchWhite, imageWidth: 15, padding: 10)
        searchBar.delegate = self
        searchBar.translatesAutoresizingMaskIntoConstraints = false
        return searchBar
    }()
    private lazy var tableView: UITableView = {
        var tableView = UITableView()
        tableView = UITableView()
        tableView.register(TracksTableViewCell.self, forCellReuseIdentifier: TracksTableViewCell.identifier)
        tableView.register(ArtistsTableViewCell.self, forCellReuseIdentifier: ArtistsTableViewCell.identifier)
        tableView.register(AlbumsTableViewCell.self, forCellReuseIdentifier: AlbumsTableViewCell.identifier)
        tableView.separatorStyle = .none
        tableView.separatorColor = ColorStruct.textFieldPlaceholder
        tableView.backgroundColor = .clear
        tableView.separatorInset = DimensionsStruct.separatorTableView
        tableView.showsVerticalScrollIndicator = false
        tableView.tableFooterView = UIView()
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 44
        tableView.delegate = self
        tableView.dataSource = self
        tableView.translatesAutoresizingMaskIntoConstraints = false
        return tableView
    }()
    // MARK: - Initialization
    init(interactor: SearchBusinessLogic, router: SearchRoutingLogic) {
        self.interactor = interactor
        self.router = router
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented in SearchViewController")
    }
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        startSettings()
    }
    
    // MARK: - Private Methods
    private func startSettings() {
    }
    
    private func configureView() {
        setupSubviews()
        setupLayout()
    }
    
    private func setupSubviews() {
        view.backgroundColor = ColorStruct.viewBackground
        view.addSubview(searchBar)
        view.addSubview(tableView)
    }
    
    private func setupLayout() {
        NSLayoutConstraint.activate([
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.topAnchor.constraint(equalTo: searchBar.bottomAnchor, constant: 20),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            searchBar.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            searchBar.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            searchBar.topAnchor.constraint(equalTo: view.topAnchor, constant: 50),
            searchBar.heightAnchor.constraint(equalToConstant: 40)
        ])
    }
    // MARK: - Interactor Methods
    func makeRequest(request: Search.All.Request.RequestType) {
        switch request {
        case .getAll(searchTerm: let searchTerm):
            interactor.makeRequest(request: .getAll(searchTerm: searchTerm))
        }
    }
}
// MARK: - SearchDisplayLogic
extension SearchViewController: SearchDisplayLogic {
    func displaySearch(viewModel: Search.All.ViewModel.ViewModelType) {
        switch viewModel {
        case .displayAll(searchViewModel: let searchViewModel):
            self.searchAllViewModel = searchViewModel
//            DispatchQueue.main.async {
                self.tableView.reloadData()
//            }
        }
    }
}

//MARK: - UISearchBarDelegate
extension SearchViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        timer?.invalidate()
        timer = Timer.scheduledTimer(withTimeInterval: 0.5, repeats: false, block: { _ in
            self.makeRequest(request: .getAll(searchTerm: searchText))
        })
    }
}
//MARK: - UISearchResultsUpdating
extension SearchViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
    }
}

//MARK: - UITableViewDataSource
extension SearchViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: TracksTableViewCell.identifier) as? TracksTableViewCell else { return UITableViewCell()}
            let track = searchAllViewModel[indexPath.row]
            cell.configure(with: track)
            return cell
        case 1:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: ArtistsTableViewCell.identifier) as? ArtistsTableViewCell else { return UITableViewCell()}
            let artist = searchAllViewModel[indexPath.row]
            cell.configure(with: artist)
            return cell
        case 2:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: AlbumsTableViewCell.identifier) as? AlbumsTableViewCell else { return UITableViewCell()}
            let track = searchAllViewModel[indexPath.row]
            cell.configure(with: track)
            return cell
        default:
            return UITableViewCell()
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return numberOfSections.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return searchAllViewModel.count - 22
        case 1:
            return searchAllViewModel.count - 22
        case 2:
            return searchAllViewModel.count - 22
        default:
            return Int()
        }
    }
}
//MARK: - UITableViewDelegate
extension SearchViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return UITableView.automaticDimension
        case 1:
            return UITableView.automaticDimension
        case 2:
            return UITableView.automaticDimension
        default:
            return CGFloat(44)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //        let model = discoverTracksViewModel[indexPath.row]
        //        router.presentPlayer(data: model)
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let customTableViewHeader = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 40))
        let sectionTitle = UILabel(frame: CGRect(x: 25, y: 0, width: customTableViewHeader.frame.size.width / 2, height: customTableViewHeader.frame.size.height / 2))
        sectionTitle.font = FontSctuct.bold18
        customTableViewHeader.addSubview(SeparatorManager.shared.setSeparatorLineFor(style: 1, width: UIApplication.shared.windows[0].frame.width))
        sectionTitle.textColor = ColorStruct.forgotLabel
        sectionTitle.text = numberOfSections[section]
        customTableViewHeader.addSubview(sectionTitle)
        return customTableViewHeader
    }
    
    // MARK: - ScrollViewDelegate
        func scrollViewDidScroll (_ scrollView: UIScrollView) {
           scrollView.setupMaskCellForScrollView(scrollView: scrollView, tableView: tableView)
        }
}


     
