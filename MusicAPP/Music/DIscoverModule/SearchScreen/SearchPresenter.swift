//
//  SearchPresenter.swift
//  MusicAPP
//
//  Created by Николай on 11.11.21.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

protocol SearchPresentationLogic: AnyObject {
    func presentData(response: Search.All.Response.ResponseType)
}

final class SearchPresenter {
    // MARK: - Properties
    weak var viewController: SearchDisplayLogic?
}

// MARK: - SearchPresentationLogic
extension SearchPresenter: SearchPresentationLogic {
    func presentData(response: Search.All.Response.ResponseType) {
        switch response {
        case .presentAll(searchResponse: let searchResponse):
           let viewModel = searchResponse?.data.map { model -> SearchViewModel in
               let cell = SearchViewModel(trackName: model.titleShort,
                                                  trackImage: model.album.coverMedium,
                                                  artistName: model.title,
                                                  albumTitle: model.album.title,
                                                  albumImage: model.album.coverMedium)
               return cell
            }
            guard let modelAll = viewModel else { return }
            viewController?.displaySearch(viewModel: .displayAll(searchViewModel: modelAll))
        }
    }
}
