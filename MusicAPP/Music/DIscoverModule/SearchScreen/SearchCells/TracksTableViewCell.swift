//
//  TracksTableViewCell.swift
//  MusicAPP
//
//  Created by Николай on 11.11.21.
//

import UIKit

class TracksTableViewCell: UITableViewCell {
    // MARK: - Variable
    static let identifier = "TracksTableViewCell"
    
    private lazy var trackImage: UIImageView = {
        var imageView = UIImageView()
        imageView.layer.cornerRadius = DimensionsStruct.cornerRadius8
        imageView.clipsToBounds = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    private lazy var trackNameLabel: UILabel = {
        var label = UILabel()
        label.textAlignment = .left
        label.textColor = ColorStruct.forgotLabel
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    private lazy var artistNameLabel: UILabel = {
        var label = UILabel()
        label.textAlignment = .left
        label.textColor = ColorStruct.textFieldPlaceholder
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    private lazy var stackViewVertical: UIStackView = {
        var stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .equalSpacing
        stackView.spacing = DimensionsStruct.spacingStackVertical
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.addArrangedSubview(trackNameLabel)
        stackView.addArrangedSubview(artistNameLabel)
        return stackView
    }()
    private lazy var buttonSheet: UIButton = {
        var button = UIButton()
        button.setImage(ImageNameStruct.buttonTable, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    lazy var spacer: UIView = {
        var view = UIView()
        return view
    }()
    // MARK: - Lifecycle function
    override func prepareForReuse() {
        super.prepareForReuse()
        trackImage.image = nil
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        contentView.addSubview(trackImage)
        contentView.addSubview(buttonSheet)
        contentView.addSubview(stackViewVertical)
        contentView.backgroundColor = ColorStruct.viewBackground
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        NSLayoutConstraint.activate([
            trackImage.widthAnchor.constraint(equalToConstant: 41),
            trackImage.heightAnchor.constraint(equalToConstant: 41),
            trackImage.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            trackImage.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 24),
            
            buttonSheet.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 18),
            buttonSheet.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -19),
            buttonSheet.leadingAnchor.constraint(equalTo: stackViewVertical.trailingAnchor, constant: 15),
            buttonSheet.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -24),

            stackViewVertical.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 8),
            stackViewVertical.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -8),
            stackViewVertical.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 90),
            stackViewVertical.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -115),
        ])
    }
}
//MARK: - ConfigurableCell
extension TracksTableViewCell: ConfigurableCell {
    typealias DataType = SearchViewModel
    func configure(with object: SearchViewModel) {
        trackNameLabel.text = object.trackName
        artistNameLabel.text = object.artistName
        let url = URL(string: object.trackImage ?? "")
        trackImage.kf.setImage(with: url)
    }
}
