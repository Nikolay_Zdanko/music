//
//  ArtistsTableViewCell.swift
//  MusicAPP
//
//  Created by Николай on 11.11.21.
//

import UIKit

class ArtistsTableViewCell: UITableViewCell {

    // MARK: - Variable
    static let identifier = "ArtistsTableViewCell"
    lazy var artistImage: UIImageView = {
        var imageView = UIImageView()
        imageView.layer.cornerRadius = DimensionsStruct.cornerRadius8
        imageView.clipsToBounds = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    lazy var artistNameLabel: UILabel = {
        var label = UILabel()
        label.textAlignment = .left
        label.textColor = ColorStruct.textFieldPlaceholder
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    lazy var buttonSheet: UIButton = {
        var button = UIButton()
        button.setImage(ImageNameStruct.buttonTable, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    // MARK: - Lifecycle function
    override func prepareForReuse() {
        super.prepareForReuse()
        artistImage.image = nil
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        contentView.addSubview(artistImage)
        contentView.addSubview(buttonSheet)
        contentView.addSubview(artistNameLabel)
        contentView.backgroundColor = ColorStruct.viewBackground
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        NSLayoutConstraint.activate([
            artistImage.widthAnchor.constraint(equalToConstant: 98),
            artistImage.heightAnchor.constraint(equalToConstant: 98),
            artistImage.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 24),
            artistImage.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 8),
            artistImage.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -8),
            
            buttonSheet.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10),
            buttonSheet.heightAnchor.constraint(equalToConstant: 20),
            buttonSheet.widthAnchor.constraint(equalToConstant: 20),
            buttonSheet.leadingAnchor.constraint(equalTo: artistNameLabel.trailingAnchor, constant: 15),
            buttonSheet.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -24),

            artistNameLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10),
//            artistNameLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -2),
            artistNameLabel.leadingAnchor.constraint(equalTo: artistImage.trailingAnchor, constant: 16),
            artistNameLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -115),
        ])
    }
}
//MARK: - ConfigurableCell
extension ArtistsTableViewCell: ConfigurableCell {
    typealias DataType = SearchViewModel
    func configure(with object: SearchViewModel) {
        artistNameLabel.text = object.artistName
        let url = URL(string: object.trackImage ?? "")
        artistImage.kf.setImage(with: url)
    }
}
