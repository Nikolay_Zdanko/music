//
//  AlbumsTableViewCell.swift
//  MusicAPP
//
//  Created by Николай on 11.11.21.
//

import UIKit

class AlbumsTableViewCell: UITableViewCell {
    // MARK: - Variable
    static let identifier = "AlbumsTableViewCell"
    lazy var albumImage: UIImageView = {
        var imageView = UIImageView()
        imageView.layer.cornerRadius = DimensionsStruct.cornerRadius8
        imageView.clipsToBounds = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    lazy var albumNameLabel: UILabel = {
        var label = UILabel()
        label.textAlignment = .left
        label.textColor = ColorStruct.textFieldPlaceholder
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    lazy var buttonSheet: UIButton = {
        var button = UIButton()
        button.setImage(ImageNameStruct.buttonTable, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    // MARK: - Lifecycle function
    override func prepareForReuse() {
        super.prepareForReuse()
        albumImage.image = nil
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        contentView.addSubview(albumImage)
        contentView.addSubview(buttonSheet)
        contentView.addSubview(albumNameLabel)
        contentView.backgroundColor = ColorStruct.viewBackground
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        NSLayoutConstraint.activate([
            albumImage.widthAnchor.constraint(equalToConstant: 98),
            albumImage.heightAnchor.constraint(equalToConstant: 98),
            albumImage.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 24),
            albumImage.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 8),
            albumImage.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -8),
            
            buttonSheet.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10),
            buttonSheet.heightAnchor.constraint(equalToConstant: 20),
            buttonSheet.widthAnchor.constraint(equalToConstant: 20),
            buttonSheet.leadingAnchor.constraint(equalTo: albumNameLabel.trailingAnchor, constant: 15),
            buttonSheet.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -24),

            albumNameLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10),
//            artistNameLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -2),
            albumNameLabel.leadingAnchor.constraint(equalTo: albumImage.trailingAnchor, constant: 16),
            albumNameLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -115),
        ])
    }
}
//MARK: - ConfigurableCell
extension AlbumsTableViewCell: ConfigurableCell {
    typealias DataType = SearchViewModel
    func configure(with object: SearchViewModel) {
        albumNameLabel.text = object.albumTitle
        let url = URL(string: object.albumImage ?? "")
        albumImage.kf.setImage(with: url)
    }
}
