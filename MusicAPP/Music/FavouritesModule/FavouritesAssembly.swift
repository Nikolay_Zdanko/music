//
//  FavouritesAssembly.swift
//  MusicAPP
//
//  Created by Николай on 8.11.21.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

final class FavouritesAssembly {
    static func assembly() -> UIViewController {
        let presenter = FavouritesPresenter()
        let router = FavouritesRouter()
        let worker = FavouritesWorker()
        let interactor = FavouritesInteractor(presenter: presenter, worker: worker )
        let viewController = FavouritesViewController(interactor: interactor, router: router)

        router.viewController = viewController
        presenter.viewController = viewController

        return viewController
    }
}
