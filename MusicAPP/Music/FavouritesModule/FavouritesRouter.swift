//
//  FavouritesRouter.swift
//  MusicAPP
//
//  Created by Николай on 8.11.21.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

protocol FavouritesRoutingLogic: AnyObject {
    func back()
}

final class FavouritesRouter: FavouritesRoutingLogic {
    // MARK: - Properties
    weak var viewController: FavouritesViewController?
    
    func back() {
        viewController?.navigationController?.popViewController(animated: true)
    }
}
