//
//  FavouritesPresenter.swift
//  MusicAPP
//
//  Created by Николай on 8.11.21.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

protocol FavouritesPresentationLogic: AnyObject {
    func presentSomething(response: Favourites.Something.Response)
}

final class FavouritesPresenter {
    // MARK: - Properties
    weak var viewController: FavouritesDisplayLogic?
}

// MARK: - FavouritesPresentationLogic
extension FavouritesPresenter: FavouritesPresentationLogic {
    func presentSomething(response: Favourites.Something.Response) {
        let viewModel = Favourites.Something.ViewModel()
        viewController?.displaySomething(viewModel: viewModel)
    }
}
