//
//  FavouritesInteractor.swift
//  MusicAPP
//
//  Created by Николай on 8.11.21.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

protocol FavouritesBusinessLogic: AnyObject {
    func doSomething(request: Favourites.Something.Request)
}

final class FavouritesInteractor {
    // MARK: - Properties
    private let presenter: FavouritesPresentationLogic
    private let worker: FavouritesWorker

    // MARK: - Initialization
    init(presenter: FavouritesPresentationLogic, worker: FavouritesWorker) {
        self.presenter = presenter
        self.worker = worker
    }
}

// MARK: - FavouritesBusinessLogic
extension FavouritesInteractor: FavouritesBusinessLogic {
    func doSomething(request: Favourites.Something.Request) {
        let response = Favourites.Something.Response()
        presenter.presentSomething(response: response)
    }
}
