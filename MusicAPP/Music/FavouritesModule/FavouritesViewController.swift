//
//  FavouritesViewController.swift
//  MusicAPP
//
//  Created by Николай on 8.11.21.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

protocol FavouritesDisplayLogic: AnyObject {
    func displaySomething(viewModel: Favourites.Something.ViewModel)
}

final class FavouritesViewController: UIViewController {
    // MARK: - Nested Types
    private enum Constants {
    }

    // MARK: - Properties
    private let interactor: FavouritesBusinessLogic
    private let router: FavouritesRoutingLogic

    // MARK: - UI Properties

    // MARK: - Initialization
    init(interactor: FavouritesBusinessLogic, router: FavouritesRoutingLogic) {
        self.interactor = interactor
        self.router = router
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented in FavouritesViewController")
    }

    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        startSettings()
    }

    // MARK: - Private Methods
    private func startSettings() {
        doSomething()
    }

    private func configureView() {
        setupSubviews()
        setupLayout()
    }

    private func setupSubviews() {
    }

    private func setupLayout() {
    }

    // MARK: - Interactor Methods
    func doSomething() {
        let request = Favourites.Something.Request()
        interactor.doSomething(request: request)
    }
}

// MARK: - FavouritesDisplayLogic
extension FavouritesViewController: FavouritesDisplayLogic {
    func displaySomething(viewModel: Favourites.Something.ViewModel) {
    }
}