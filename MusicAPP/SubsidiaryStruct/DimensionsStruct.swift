//
//  DimensionsStruct.swift
//  music
//
//  Created by Николай on 19.10.21.
//

import UIKit

struct DimensionsStruct {
    static let cornerRadius: CGFloat = 10
    static let imageWidth: CGFloat = 20
    static let padding: CGFloat = 16
    static let spacingForTextField: CGFloat = 24
    static let spacingForButton: CGFloat = 16
    static let spacingForButtonDontAccauntSignI: CGFloat = 8
    static let borderWidth: CGFloat = 1.5
    static let cornerRadius8: CGFloat = 8
    static let separatorTableView: UIEdgeInsets = UIEdgeInsets(top: 0, left: 24, bottom: 0, right: 24)
    static let heightForDiscoverTableView: CGFloat = 57
    static let spacingStackVertical: CGFloat = 2
}

struct PlaylistCollectionConstants {
    static let heightForHeader: CGFloat = 50
    static let playlistsCollectionLeftSpacing:CGFloat = 16
    static let playlistsCollectionRightSpacing:CGFloat = 16
    static let playlistsCollectionInterItemSpacing:CGFloat = 16
    static let playlistsCollectionTopItemSpacing:CGFloat = 16
    static let playlistsCollectionBottomItemSpacing:CGFloat = 16
    static let playlistImageRadius:CGFloat = 20
}

struct AlbumsCollectionConstants {
    static let heightForHeader: CGFloat = 50
    static let albumCollectionLeftSpacing: CGFloat = 16
    static let albumCollectionRightSpacing: CGFloat = 16
    static let albumCollectionInterItemSpacing: CGFloat = 16
    static let albumCollectionTopItemSpacing :CGFloat = 16
    static let albumCollectionBottomItemSpacing: CGFloat = 16
    static let albumImageRadius: CGFloat = 20
}
