//
//  ImageNameStruct.swift
//  music
//
//  Created by Николай on 14.10.21.
//

import UIKit

struct ImageNameStruct {
    static let logo = UIImage(named: "logo")
    static let eye = UIImage(named: "eye")
    static let eyeBlack = UIImage(named: "eyeBlack")
    static let lock = UIImage(named: "lock")
    static let lockBlack = UIImage(named: "lockBlack")
    static let line = UIImage(named: "line")
    static let imageAvatar = UIImage(named: "imageAvatar")
    static let button = UIImage(named: "button")
    static let vector = UIImage(named: "vector")
    static let checkmark = UIImage(named: "Checkmark")
    static let checkbox = UIImage(named: "checkbox")
    static let emptyCheckmark = UIImage(named: "Checkmarkempty")
    static let mail = UIImage(named: "mail")
    static let mailBlack = UIImage(named: "mailBlack")
    static let google = UIImage(named: "google")
    static let verification = UIImage(named: "verification")
    static let crossedEye = UIImage(named: "crossedEye")
    static let crossedEyeBlack = UIImage(named: "crossedEyeBlack")
    static let agreeTermsErrorImage = UIImage(named: "agreeTermsErrorImage")
    static let pen = UIImage(named: "pen")
    static let search = UIImage(named: "search")
    static let defaultImage = UIImage(named: "defaultImage")
    static let buttonTable = UIImage(named: "buttonTable")
    static let album = UIImage(named: "album")
    static let favourites = UIImage(named: "favourites")
    static let profile = UIImage(named: "profile")
    static let atrist = UIImage(named: "atrist")
    static let profileBlack = UIImage(named: "profileBlack")
    static let searchWhite = UIImage(named: "searchWhite")
    static let playButton = UIImage(named: "PlayButton")
    static let emptyLike = UIImage(named: "emptyLike")
}
