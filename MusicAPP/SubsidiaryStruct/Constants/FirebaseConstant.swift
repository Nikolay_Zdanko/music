//
//  FirebaseConstants.swift
//  MusicAPP
//
//  Created by Егор Евсеенко on 21.10.2021.
//

import Foundation

struct FirebaseConstant{
    static let uid = "uid"
    static let email = "email"
    static let username = "username"
    static let profileImageUrl = "profileImageUrl"
    static let users = "users"
    static let storageURL = "gs://musica-84523.appspot.com"
    static let profilePhoto = "profilePhoto"
    static let profilePhotoContentType = "image/jpg"
    static let googleClientID = "847734758320-fietmfpek2qmnnr9qa9pte98vo584gci.apps.googleusercontent.com"
}
