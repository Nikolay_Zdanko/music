//
//  Font.swift
//  MusicAPP
//
//  Created by Егор Евсеенко on 19.10.2021.
//

import Foundation

struct Font{
    static let mainBold = "SFUIDisplay-Bold"
    static let mainNormal = "SFUIDisplay-Normal"
}
