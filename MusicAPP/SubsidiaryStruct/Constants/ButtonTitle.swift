//
//  ButtonTitle.swift
//  MusicAPP
//
//  Created by Егор Евсеенко on 19.10.2021.
//

import Foundation

struct ButtonTitle{
    static let skip = "Skip"
    static let next = "Next"
    static let signUp = "Sign Up"
    static let forgotYourPassword = "Forgot your password"
    static let OK = "OK"
    static let forgotPassword = "Send reset link"
}
