//
//  ColorStruct.swift
//  music
//
//  Created by Николай on 19.10.21.
//

import UIKit

struct ColorStruct {
    static let forgotLabel = UIColor(red: 0.929, green: 0.949, blue: 0.957, alpha: 1)
    static let textFieldColor = UIColor(red: 0.304, green: 0.335, blue: 0.429, alpha: 1)
    static let textFieldPlaceholder = UIColor(red: 0.553, green: 0.6, blue: 0.682, alpha: 1)
    static let buttonSignIn = UIColor(red: 0.851, green: 0.016, blue: 0.161, alpha: 1)
    static let viewBackground = UIColor(red: 0.176, green: 0.122, blue: 0.208, alpha: 1)
    static let textForTextView = UIColor(red: 0.557273984, green: 0.5973963141, blue: 0.6820154786, alpha: 1)
    static let navigationBar = UIColor(red: 0.176, green: 0.122, blue: 0.208, alpha: 1)
    static let errorTextFieldColor = CGColor(red: 0.858, green: 0.25, blue: 0.356, alpha: 1)
    static let agreeTermsError = UIColor(red: 0.858, green: 0.25, blue: 0.356, alpha: 1)
    static let albumDetailsHeader = UIColor(red: 0.929, green: 0.949, blue: 0.957, alpha: 1)
    static let artiscOfAlbum = UIColor(red: 0.553, green: 0.6, blue: 0.682, alpha: 1)
}
