//
//  FontNameSctuct.swift
//  music
//
//  Created by Николай on 19.10.21.
//

import UIKit

struct FontSctuct {
    static let normal11 = UIFont(name: "SFUIDisplay-Normal", size: 11)
    static let normal15 = UIFont(name: "SFUIDisplay-Normal", size: 15)
    static let regular14 = UIFont(name: "SFUIDisplay-Regular", size: 14)
    static let regular12 = UIFont(name: "SFUIDisplay-Regular", size: 12)
    static let medium14 = UIFont(name: "SFUIDisplay-Medium", size: 14)
    static let bold14 = UIFont(name: "SFUIDisplay-Bold", size: 14)
    static let bold16 = UIFont(name: "SFUIDisplay-Bold", size: 16)
    static let bold17 = UIFont(name: "SFUIDisplay-Bold", size: 17)
    static let bold18 = UIFont(name: "SFUIDisplay-Bold", size: 18)
    static let bold24 = UIFont(name: "SFUIDisplay-Bold", size: 24)
    static let bold35 = UIFont(name: "SFUIDisplay-Bold", size: 35)
    static let semibold17 = UIFont(name: "SFUIDisplay-Semibold", size: 17)
}
