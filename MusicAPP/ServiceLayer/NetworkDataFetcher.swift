//
//  NetworkDataFetcher.swift
//  getRequest
//
//  Created by Николай on 12.10.21.
//

import Foundation

class NetworkDataFetcher {
    
    var networkService: NetworkService?
    
    init(networkService: NetworkService = NetworkService()) {
        self.networkService = networkService
    }
    
    //декодируем полученные JSON данные в конткретную модель данных
    func fetchGenericJSONData<T:Decodable>(urlString: String, response: @escaping (T?) -> Void) {
        networkService?.request(urlString: urlString) { data, error in
            if let error = error {
                print(error.localizedDescription)
                response(nil)
            }
            let decoded = self.decodeJSON(type: T.self, from: data)
            response(decoded)
        }
    }
    
   private func decodeJSON<T: Decodable>(type: T.Type, from: Data?) -> T? {
        let decoder = JSONDecoder()
        guard let data = from else { return nil}
        do {
            let objects = try decoder.decode(type.self, from: data)
            return objects
        } catch let jsonError {
            print(jsonError.localizedDescription)
            return nil
        }
    }
    
}
