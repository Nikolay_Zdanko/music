//
//  DataFetcherService.swift
//  getRequest
//
//  Created by Николай on 12.10.21.
//

import Foundation

protocol DataFetcherServiceProtocol {
    func fetchData(completion: @escaping (DeezerModel?) -> Void)
    func searchData(searchText: String, completion: @escaping (SearchModel?) -> Void)
}

class DataFetcherService: DataFetcherServiceProtocol {
    var networkDataFetcher: NetworkDataFetcher?
    
    init(networkDataFetcher: NetworkDataFetcher = NetworkDataFetcher()) {
        self.networkDataFetcher = networkDataFetcher
    }
    
    func fetchData(completion: @escaping (DeezerModel?) -> Void) {
        let urlTopTrack = "https://api.deezer.com/chart"
        networkDataFetcher?.fetchGenericJSONData(urlString: urlTopTrack, response: completion)
    }
    
    func searchData(searchText: String, completion: @escaping (SearchModel?) -> Void) {
        let searchUrl = "https://api.deezer.com/search?q=\(searchText)"
        networkDataFetcher?.fetchGenericJSONData(urlString: searchUrl, response: completion)
    }
}
