//
//  TermsOfUseViewController.swift
//  music
//
//  Created by Николай on 18.10.21.
//

import UIKit

class TermsOfUseViewController: UIViewController {
    //MARK: - UI elements in code
    private lazy var label: UILabel = {
        var label = UILabel()
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        let firstText = TermsText.firstText
        let firstAttributes = [NSAttributedString.Key.font: FontSctuct.bold16]
        var attributesString = NSMutableAttributedString(string: firstText, attributes: firstAttributes as [NSAttributedString.Key : Any])
        let secondText = TermsText.secondText
        let secondAttributes = [NSAttributedString.Key.font: FontSctuct.regular14]
        let secondAttributesString = NSMutableAttributedString(string: secondText, attributes: secondAttributes as [NSAttributedString.Key : Any])
        attributesString.append(secondAttributesString)
        let thirdText = TermsText.thirdText
        let thirdAttributes = [NSAttributedString.Key.font: FontSctuct.bold16]
        let thirdAttributesString = NSMutableAttributedString(string: thirdText, attributes: thirdAttributes as [NSAttributedString.Key : Any])
        attributesString.append(thirdAttributesString)
        let fourthText = TermsText.fourthText
        let fourthAttributes = [NSAttributedString.Key.font: FontSctuct.regular14]
        let fourthAttributesString = NSMutableAttributedString(string: fourthText, attributes: fourthAttributes as [NSAttributedString.Key : Any])
        attributesString.append(fourthAttributesString)
        let fifthText = TermsText.fifthText
        let fifthAttributes = [NSAttributedString.Key.font: FontSctuct.bold16]
        let fifthAttributesString = NSMutableAttributedString(string: fifthText, attributes: fifthAttributes as [NSAttributedString.Key : Any])
        attributesString.append(fifthAttributesString)
        let sixthText = TermsText.sixthText
        let sixthAttributes = [NSAttributedString.Key.font: FontSctuct.regular14]
        let sixthAttributesString = NSMutableAttributedString(string: sixthText, attributes: sixthAttributes as [NSAttributedString.Key : Any])
        attributesString.append(sixthAttributesString)
        label.attributedText = attributesString
        return label
    }()
    
    private lazy var contentViewSize = CGSize(width: self.view.frame.width, height: self.view.frame.height + (self.view.frame.height / 5.275))
    private lazy var scrollView: UIScrollView = {
        let view = UIScrollView(frame: .zero)
        view.backgroundColor = ColorStruct.forgotLabel
        view.frame = self.view.bounds
        view.contentSize = contentViewSize
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    private lazy var containerView: UIView = {
        let v = UIView()
        v.backgroundColor = .white
        v.frame.size = contentViewSize
        return v
    }()
    //MARK:- Lifecycle functions
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.barTintColor = .white
        navigationController?.navigationBar.tintColor = .black
        navigationController?.navigationBar.backItem?.title = ""
        title = TermsText.title
        view.backgroundColor = ColorStruct.forgotLabel
        view.addSubview(scrollView)
        scrollView.addSubview(containerView)
        setupContainer(containerView)
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        setupConstraintForScrollView()
    }
    //MARK:- Flow function
    private func setupContainer(_ container: UIView) {
        container.addSubview(label)
        label.topAnchor.constraint(equalTo: container.topAnchor, constant: view.frame.height / 70.3).isActive = true
        label.leadingAnchor.constraint(equalTo: container.leadingAnchor, constant: view.frame.height / 46.8).isActive = true
        label.trailingAnchor.constraint(equalTo: container.trailingAnchor, constant: view.frame.height / -28.13).isActive = true
        label.bottomAnchor.constraint(equalTo: container.bottomAnchor, constant: view.frame.height / 70.3).isActive = true
    }
    private func setupConstraintForScrollView() {
        scrollView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        scrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }
}
