//
//  PrivacyPolicyViewController.swift
//  music
//
//  Created by Николай on 18.10.21.
//

import UIKit

class PrivacyPolicyViewController: UIViewController {
    //MARK: - UI elements in code
    private lazy var label: UILabel = {
        var label = UILabel()
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        let firstText = PrivacyText.firstText
        let firstAttributes = [NSAttributedString.Key.font: FontSctuct.regular14]
        var attributesString = NSMutableAttributedString(string: firstText, attributes: firstAttributes as [NSAttributedString.Key : Any])
        let secondText = PrivacyText.secondText
        let secondAttributes = [NSAttributedString.Key.font: FontSctuct.bold16]
        let secondAttributesString = NSMutableAttributedString(string: secondText, attributes: secondAttributes as [NSAttributedString.Key : Any])
        attributesString.append(secondAttributesString)
        let thirdText = PrivacyText.thirdText
        let thirdAttributes = [NSAttributedString.Key.font: FontSctuct.regular14]
        let thirdAttributesString = NSMutableAttributedString(string: thirdText, attributes: thirdAttributes as [NSAttributedString.Key : Any])
        attributesString.append(thirdAttributesString)
        label.attributedText = attributesString
        return label
    }()
    private lazy var contentViewSize = CGSize(width: self.view.frame.width, height: self.view.frame.height + (view.frame.height / 1.46))
    lazy var scrollView: UIScrollView = {
        let view = UIScrollView(frame: .zero)
        view.backgroundColor = .white
        view.frame = self.view.bounds
        view.contentSize = contentViewSize
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    lazy var containerView: UIView = {
        let v = UIView()
        v.backgroundColor = ColorStruct.forgotLabel
        v.frame.size = contentViewSize
        return v
    }()
    //MARK:- Lifecycle functions
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.backItem?.title = ""
        navigationController?.navigationBar.barTintColor = .white
        navigationController?.navigationBar.tintColor = .black
        title = PrivacyText.title
        view.backgroundColor = ColorStruct.forgotLabel
        view.addSubview(scrollView)
        scrollView.addSubview(containerView)
        setupContainer(containerView)
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        setupConstraintForScrollView()
    }
    //MARK:- Flow function
    private func setupContainer(_ container: UIView) {
        container.addSubview(label)
        label.topAnchor.constraint(equalTo: container.topAnchor, constant: view.frame.height / 70.333).isActive = true
        label.leadingAnchor.constraint(equalTo: container.leadingAnchor, constant: view.frame.height / 46.888).isActive = true
        label.trailingAnchor.constraint(equalTo: container.trailingAnchor, constant: view.frame.height / -28.133).isActive = true
        label.bottomAnchor.constraint(equalTo: container.bottomAnchor, constant: view.frame.height / 70.333).isActive = true
    }
    private func setupConstraintForScrollView() {
        scrollView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        scrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }
}
