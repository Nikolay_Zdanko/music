//
//  SingInviaGoogleViewController.swift
//  music
//
//  Created by Николай on 20.10.21.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase
import FirebaseStorage
import FirebaseAuth
import Kingfisher

class SignUpViaGoogleViewController: UIViewController {
    var email: String?
    var username: String?
    var uuid: String?
    var profilePhoto: URL?
    
    //MARK: - UI elements in code
    private lazy var agreeTermsErrorView: UIView = {
        var view = UIView()
        view.isHidden = true
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = ColorStruct.agreeTermsError
        return view
    }()
    private lazy var agreeTermsErrorImageView: UIImageView = {
        var imageView = UIImageView()
        imageView.image = ImageNameStruct.agreeTermsErrorImage
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.layer.masksToBounds = true
        return imageView
    }()
    private lazy var agreeTermsErrorLabel: UILabel = {
        var label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.adjustsFontSizeToFitWidth = true
        label.text = TextStruct.agreeTermsError
        label.contentMode = .right
        label.textColor = ColorStruct.forgotLabel
        label.font = FontSctuct.regular12
        return label
    }()
    private lazy var agreeTermsFirst: UIStackView = {
        var stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.spacing = DimensionsStruct.spacingForTextField
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.addArrangedSubview(agreeTermsErrorImageView)
        stackView.addArrangedSubview(agreeTermsErrorLabel)
        return stackView
    }()
    private lazy var avatarImageView: UIImageView = {
        var imageView = UIImageView()
        imageView.image = ImageNameStruct.imageAvatar
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.clipsToBounds = true
        return imageView
    }()
    private lazy var buttonAdd: UIButton = {
        var button = UIButton()
        button.setImage(ImageNameStruct.button, for: .normal)
        button.setImage(ImageNameStruct.pen, for: .selected)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(presentPicker(_:)), for: .touchUpInside)
        return button
    }()
    private lazy var userTextField: UITextField = {
        var textField = UITextField()
        textField.tag = 0
        textField.leftView(ImageNameStruct.profile, imageWidth: DimensionsStruct.imageWidth, padding: DimensionsStruct.padding)
        textField.backgroundColor = ColorStruct.textFieldColor
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.layer.cornerRadius = DimensionsStruct.cornerRadius
        textField.font = FontSctuct.regular14
        textField.attributedPlaceholder = NSAttributedString(string: TextStruct.userTextField,
                                                                 attributes: [NSAttributedString.Key.foregroundColor: ColorStruct.textFieldPlaceholder])
        return textField
    }()
    private lazy var termsAndPrivacyLabel: UILabel = {
        var label = UILabel()
        label.textAlignment = .left
        label.backgroundColor = .clear
        label.textColor = ColorStruct.textForTextView
        let attributedString = NSMutableAttributedString(string: TextStruct.textView)
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.white , range: NSRange(location: 15, length: 13))
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.white , range: NSRange(location: 31, length: 15))
        label.attributedText = attributedString
        label.font = FontSctuct.regular14
        label.translatesAutoresizingMaskIntoConstraints = false
        label.isUserInteractionEnabled = true
        label.adjustsFontSizeToFitWidth = true
        label.addGestureRecognizer(UITapGestureRecognizer(target:self, action: #selector(termsAndPrivacyGesture(gesture:))))
        return label
    }()
    private lazy var buttonCheckBox: UIButton = {
        var button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(ImageNameStruct.checkbox, for: .normal)
        button.setImage(ImageNameStruct.checkmark, for: .selected)
        button.addTarget(self, action: #selector(checkBoxClicked(_:)), for: .touchUpInside)
        return button
    }()
    private lazy var stackVertical: UIStackView = {
        var stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .equalSpacing
        stackView.spacing = DimensionsStruct.spacingForButtonDontAccauntSignI
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.addArrangedSubview(buttonSignIn)
        stackView.addArrangedSubview(stackViewForButtonDontAccauntSignIn)
        return stackView
    }()
    private lazy var stackViewForButtonDontAccauntSignIn: UIStackView = {
        var stackView  = UIStackView()
        stackView.axis = .horizontal
        stackView.distribution = .fill
        stackView.spacing = DimensionsStruct.spacingForButtonDontAccauntSignI
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.addArrangedSubview(buttonDontAccaunt)
        stackView.addArrangedSubview(buttonDontAccauntSignIn)
        return stackView
    }()
    private lazy var buttonSignIn: UIButton = {
        var button = UIButton()
        button.backgroundColor = ColorStruct.buttonSignIn
        button.setTitle(TextStruct.buttonSignIn, for: .normal)
        button.titleLabel?.font = FontSctuct.medium14
        button.setTitleColor(ColorStruct.forgotLabel, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.layer.cornerRadius = DimensionsStruct.cornerRadius
        button.addTarget(self, action: #selector(signInClicked(_:)), for: .touchUpInside)
        return button
    }()
    private lazy var buttonDontAccaunt: UIButton = {
        var button = UIButton()
        button.setTitle(TextStruct.buttonDontAccaunt, for: .normal)
        button.setTitleColor(ColorStruct.textFieldPlaceholder,for: .normal)
        button.titleLabel?.font = FontSctuct.regular14
        return button
    }()
    private lazy var buttonDontAccauntSignIn: UIButton = {
        var button = UIButton()
        button.setTitle(TextStruct.buttonSignIn, for: .normal)
        button.setTitleColor(ColorStruct.forgotLabel,for: .normal)
        button.titleLabel?.font = FontSctuct.regular14
        button.addTarget(self, action: #selector(buttonPresent), for: .touchUpInside)
        return button
    }()
    //MARK: - Lifecycle functions
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = ColorStruct.viewBackground
        addSubViewForView()
        setInfo()
        userTextField.delegate = self
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(hideKeyboardRecognizerClicked(_:)))
        view.addGestureRecognizer(tapRecognizer)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavigationBar()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        setupConstraintAgreeTermsFirst()
        setupConstraintForAvatarImageView()
        setupConstraintForTextField()
        setupConstraintForButtonAdd()
        setupConstraintForCheckBox()
        setupConstraintForStackVertical()
        setupConstraintForButtonDontSignIn()
    }
    //MARK: - IBAction
    @objc
    private func hideKeyboardRecognizerClicked(_ sender: UITapGestureRecognizer) {
        view.endEditing(true)
    }
    @objc
    private func checkBoxClicked(_ sender: Any){
        buttonCheckBox.isSelected.toggle()
    }
    @objc
    private func buttonPresent(_ sender: UIButton) {
        let signUpViewController = SignUpViewController()
        self.navigationController?.pushViewController(signUpViewController, animated: true)
    }
    @objc
    private func termsAndPrivacyGesture(gesture: UITapGestureRecognizer) {
        guard let termsOfUse = (termsAndPrivacyLabel.text as NSString?)?.range(of: TextStruct.termsOfUseButton) else { return  }
        guard let privacyPolicy = (termsAndPrivacyLabel.text as NSString?)?.range(of: TextStruct.privacyPolicyButton) else { return  }
        
        if gesture.didTapAttributedTextInLabel(label: termsAndPrivacyLabel, inRange: termsOfUse) {
            //            let terms = ForgotViewController()
            //            navigationController?.pushViewController(terms, animated: true)
        } else if gesture.didTapAttributedTextInLabel(label: termsAndPrivacyLabel, inRange: privacyPolicy) {
            //            navigationController?.pushViewController(PrivacyPolicyViewController(), animated: true)
        }
    }
    @objc
    private func signInClicked(_ sender: UIButton) {
        if buttonCheckBox.isSelected {
            navigationController?.setNavigationBarHidden(false, animated: false)
            agreeTermsErrorView.isHidden = true
        } else {
            navigationController?.setNavigationBarHidden(true, animated: false)
            agreeTermsErrorView.isHidden = false
            return
        }
        var image = Data()
        guard let email = email,
              let uuid = uuid ,
              let profilePhoto = profilePhoto,
              let username = termsAndPrivacyLabel.text else { return }
        var dict: Dictionary <String, Any> = [
            FirebaseConstant.uid : uuid,
            FirebaseConstant.email : email,
            FirebaseConstant.username  : username,
            FirebaseConstant.profileImageUrl  : profilePhoto
        ]
        let storageRef = Storage.storage().reference(forURL: FirebaseConstant.storageURL)
        let storageProfileRef = storageRef.child(FirebaseConstant.profilePhoto).child(uuid)
        let metadata = StorageMetadata()
        metadata.contentType = FirebaseConstant.profilePhotoContentType
        guard let imageData = self.avatarImageView.image?.jpegData(compressionQuality: 0.4) else { return }
        image = imageData
        storageProfileRef.putData(image, metadata: metadata) { (storageMetaData, error) in
            if error != nil{
                return
            }
            storageProfileRef.downloadURL { (url, error) in
                if let metaImageUrl = url?.absoluteString{
                    print(metaImageUrl)
                    dict[FirebaseConstant.profileImageUrl] = metaImageUrl
                    Database.database().reference().child(FirebaseConstant.users).child(uuid).updateChildValues(dict) { [weak self] error, ref in
                        guard let self = self,
                              let error = error else { return }
                    }
                }
            }
        }
        self.navigationController?.pushViewController(OnboardingViewController(), animated: true)
    }
    //MARK: - Flow function
    private func addSubViewForView() {
        view.addSubview(agreeTermsErrorView)
        agreeTermsErrorView.addSubview(agreeTermsFirst)
        view.addSubview(avatarImageView)
        view.addSubview(buttonAdd)
        view.addSubview(userTextField)
        view.addSubview(buttonCheckBox)
        view.addSubview(termsAndPrivacyLabel)
        view.addSubview(stackVertical)
        view.addSubview(stackViewForButtonDontAccauntSignIn)
    }
    private func setupNavigationBar() {
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.backItem?.title = ""
        self.navigationController?.navigationBar.tintColor = .white
        self.navigationController?.navigationBar.barTintColor = ColorStruct.navigationBar
    }
    private func setInfo(){
        guard let email = email,
              let uuid = uuid ,
              let profilePhoto = profilePhoto else { return }
        avatarImageView.kf.setImage(with: profilePhoto)
    }
    private func setupConstraintAgreeTermsFirst(){
        agreeTermsErrorView.widthAnchor.constraint(equalToConstant: view.frame.width).isActive = true
        agreeTermsErrorView.heightAnchor.constraint(equalToConstant: 55).isActive = true
        agreeTermsErrorView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0).isActive = true
        agreeTermsErrorView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 0).isActive = true
        agreeTermsErrorImageView.widthAnchor.constraint(equalToConstant: 24).isActive = true
        agreeTermsErrorImageView.heightAnchor.constraint(equalToConstant: 24).isActive = true
        agreeTermsFirst.topAnchor.constraint(equalTo: agreeTermsErrorView.topAnchor, constant: 16).isActive = true
        agreeTermsFirst.bottomAnchor.constraint(equalTo: agreeTermsErrorView.bottomAnchor, constant: -16).isActive = true
        agreeTermsFirst.leadingAnchor.constraint(equalTo: agreeTermsErrorView.leadingAnchor, constant: 30).isActive = true
        agreeTermsFirst.trailingAnchor.constraint(equalTo: agreeTermsErrorView.trailingAnchor, constant: -30).isActive = true
    }
    private func setupConstraintForButtonAdd() {
        buttonAdd.trailingAnchor.constraint(equalTo: avatarImageView.trailingAnchor, constant: view.frame.height / -281.3).isActive = true
        buttonAdd.bottomAnchor.constraint(equalTo: avatarImageView.bottomAnchor, constant: view.frame.height / -281.3).isActive = true
        buttonAdd.widthAnchor.constraint(equalToConstant: view.frame.height / 33.76).isActive = true
        buttonAdd.heightAnchor.constraint(equalToConstant: view.frame.height / 33.76).isActive = true
    }
    private func setupConstraintForAvatarImageView() {
        avatarImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        avatarImageView.topAnchor.constraint(equalTo: view.topAnchor, constant: view.frame.height / 5.9).isActive = true
        avatarImageView.heightAnchor.constraint(equalToConstant: view.frame.height / 8.53).isActive = true
        avatarImageView.widthAnchor.constraint(equalToConstant: view.frame.height / 8.53).isActive = true
        avatarImageView.layer.cornerRadius = avatarImageView.frame.width / 2
    }
    private func setupConstraintForTextField() {
        userTextField.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        userTextField.topAnchor.constraint(equalTo: avatarImageView.bottomAnchor, constant: view.frame.height / 8.61).isActive = true
        userTextField.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: view.frame.height / 35.16).isActive = true
        userTextField.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: view.frame.height / -35.16).isActive = true
        userTextField.heightAnchor.constraint(equalToConstant: view.frame.height / 21.1).isActive = true
    }
    private func setupConstraintForCheckBox() {
        termsAndPrivacyLabel.topAnchor.constraint(equalTo: userTextField.bottomAnchor, constant: view.frame.height / 33.76).isActive = true
        termsAndPrivacyLabel.heightAnchor.constraint(equalToConstant: view.frame.height / 46.88).isActive = true
        termsAndPrivacyLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: view.frame.height / -17.583).isActive = true
        termsAndPrivacyLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: view.frame.height / 16.23).isActive = true
        buttonCheckBox.widthAnchor.constraint(equalToConstant: view.frame.height / 42.2).isActive = true
        buttonCheckBox.heightAnchor.constraint(equalToConstant: view.frame.height / 42.2).isActive = true
        buttonCheckBox.topAnchor.constraint(equalTo: userTextField.bottomAnchor, constant: view.frame.height / 35.76).isActive = true
        buttonCheckBox.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: view.frame.height / 35.16).isActive = true
    }
    private func setupConstraintForStackVertical() {
        stackVertical.topAnchor.constraint(equalTo: termsAndPrivacyLabel.bottomAnchor,constant: view.frame.height / 3.36).isActive = true
        stackVertical.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: view.frame.height / 35.16).isActive = true
        stackVertical.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: view.frame.height / -35.16).isActive = true
        buttonSignIn.heightAnchor.constraint(equalToConstant: view.frame.height / 18.347).isActive = true
    }
    private func setupConstraintForButtonDontSignIn() {
        stackViewForButtonDontAccauntSignIn.topAnchor.constraint(equalTo: stackVertical.bottomAnchor, constant: view.frame.height / 52.75).isActive = true
        stackViewForButtonDontAccauntSignIn.centerXAnchor.constraint(equalTo: stackVertical.centerXAnchor).isActive = true
    }
}
//MARK: - PickerPresenter
extension SignUpViaGoogleViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let imageSelected = info[UIImagePickerController.InfoKey.editedImage] as? UIImage{
            avatarImageView.image = imageSelected
        }
        if let imageOriginal = info[UIImagePickerController.InfoKey.originalImage] as? UIImage{
            avatarImageView.image = imageOriginal
        }
        buttonAdd.isSelected = true
        picker.dismiss(animated: true, completion: nil)
    }
    @objc
    private func presentPicker(_ sender: Any){
        let picker = UIImagePickerController()
        UINavigationBar.appearance().tintColor = ColorStruct.viewBackground
        picker.sourceType = .photoLibrary
        picker.allowsEditing = true
        picker.delegate = self
        picker.modalPresentationStyle = .fullScreen
        self.present(picker, animated: true, completion: nil)
    }
}
//MARK: - UITextFieldDelegate
extension SignUpViaGoogleViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        switch textField.tag {
        case 0:
            textField.leftViewMode = .always
            textField.leftView(ImageNameStruct.profileBlack, imageWidth: DimensionsStruct.imageWidth, padding: DimensionsStruct.padding)
            textField.textColor =  ColorStruct.textFieldColor
            textField.backgroundColor = ColorStruct.textFieldPlaceholder
            textField.attributedPlaceholder = NSAttributedString(string: TextStruct.userTextField,
                                                                         attributes: [NSAttributedString.Key.foregroundColor: ColorStruct.textFieldColor])
        default:
            break
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField.tag {
        case 0:
            textField.leftView(ImageNameStruct.profile, imageWidth: DimensionsStruct.imageWidth, padding: DimensionsStruct.padding)
            textField.textColor =  ColorStruct.textFieldPlaceholder
            textField.backgroundColor = ColorStruct.textFieldColor
            textField.attributedPlaceholder = NSAttributedString(string: TextStruct.userTextField,
                                                                     attributes: [NSAttributedString.Key.foregroundColor: ColorStruct.textFieldPlaceholder])
        default:
            break
        }
    }
}
