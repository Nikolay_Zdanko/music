//
//  SignUpViewController.swift
//  music
//
//  Created by Николай on 13.10.21.
//

import UIKit
import Firebase
import FirebaseDatabase
import FirebaseStorage

class SignUpViewController: UIViewController {
    //MARK: - UI elements in code
    private lazy var agreeTermsErrorView: UIView = {
        var view = UIView()
        view.isHidden = true
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = ColorStruct.agreeTermsError
        return view
    }()
    private lazy var agreeTermsErrorImageView: UIImageView = {
        var imageView = UIImageView()
        imageView.image = ImageNameStruct.agreeTermsErrorImage
        imageView.contentMode = .scaleAspectFill
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.layer.masksToBounds = true
        return imageView
    }()
    private lazy var agreeTermsErrorLabel: UILabel = {
        var label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.adjustsFontSizeToFitWidth = true
        label.text = TextStruct.agreeTermsError
        label.contentMode = .right
        label.textColor = ColorStruct.forgotLabel
        label.font = FontSctuct.regular12
        return label
    }()
    private lazy var agreeTermsFirst: UIStackView = {
        var stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.spacing = DimensionsStruct.spacingForTextField
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.addArrangedSubview(agreeTermsErrorImageView)
        stackView.addArrangedSubview(agreeTermsErrorLabel)
        return stackView
    }()
    private lazy var avatarImageView: UIImageView = {
        var imageView = UIImageView()
        imageView.image = ImageNameStruct.imageAvatar
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.layer.masksToBounds = true
        return imageView
    }()
    private lazy var buttonAdd: UIButton = {
        var button = UIButton()
        button.setImage(ImageNameStruct.button, for: .normal)
        button.setImage(ImageNameStruct.pen, for: .selected)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(presentPicker(_:)), for: .touchUpInside)
        return button
    }()
    private lazy var emailTextField: UITextField = {
        var textField = UITextField()
        textField.tag = 0
        textField.leftView(ImageNameStruct.mail, imageWidth: DimensionsStruct.imageWidth, padding: DimensionsStruct.padding)
        textField.backgroundColor = ColorStruct.textFieldColor
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.layer.cornerRadius = DimensionsStruct.cornerRadius
        textField.font = FontSctuct.regular14
        textField.attributedPlaceholder = NSAttributedString(string: TextStruct.emailTextField,
                                                                     attributes: [NSAttributedString.Key.foregroundColor: ColorStruct.textFieldPlaceholder])
        return textField
    }()
    private lazy var passwordTextField: UITextField = {
        var textField = UITextField()
        textField.tag = 1
        textField.leftView(ImageNameStruct.lock, imageWidth: DimensionsStruct.imageWidth, padding: DimensionsStruct.padding)
        textField.rightViewMode = .always
        textField.isSecureTextEntry = true
        textField.rightView = buttonSecurity
        textField.rightView?.widthAnchor.constraint(equalToConstant: view.frame.height / 18.76).isActive = true
        textField.backgroundColor = ColorStruct.textFieldColor
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.layer.cornerRadius = DimensionsStruct.cornerRadius
        textField.font = FontSctuct.regular14
        textField.attributedPlaceholder = NSAttributedString(string: TextStruct.passwordTextField,
                                                                     attributes: [NSAttributedString.Key.foregroundColor: ColorStruct.textFieldPlaceholder])
        return textField
    }()
    private lazy var confirmPasswordTextField: UITextField = {
        var textField = UITextField()
        textField.tag = 2
        textField.leftView(ImageNameStruct.lock, imageWidth: DimensionsStruct.imageWidth, padding: DimensionsStruct.padding)
        textField.rightViewMode = .always
        textField.isSecureTextEntry = true
        textField.rightView = buttonConfirmSecurity
        textField.rightView?.widthAnchor.constraint(equalToConstant: view.frame.height / 18.76).isActive = true
        textField.backgroundColor = ColorStruct.textFieldColor
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.layer.cornerRadius = DimensionsStruct.cornerRadius
        textField.font = FontSctuct.regular14
        textField.attributedPlaceholder = NSAttributedString(string: TextStruct.confirmPassword,
                                                                            attributes: [NSAttributedString.Key.foregroundColor: ColorStruct.textFieldPlaceholder])
        return textField
    }()
    private lazy var userTextField: UITextField = {
        var textField = UITextField()
        textField.tag = 3
        textField.leftView(ImageNameStruct.profile, imageWidth: DimensionsStruct.imageWidth, padding: DimensionsStruct.padding)
        textField.backgroundColor = ColorStruct.textFieldColor
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.layer.cornerRadius = DimensionsStruct.cornerRadius
        textField.font = FontSctuct.regular14
        textField.attributedPlaceholder = NSAttributedString(string: TextStruct.userTextField,
                                                                 attributes: [NSAttributedString.Key.foregroundColor: ColorStruct.textFieldPlaceholder])
        return textField
    }()
    private lazy var stackViewForTextField: UIStackView = {
        var stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .fillEqually
        stackView.spacing = DimensionsStruct.spacingForTextField
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.addArrangedSubview(emailTextField)
        stackView.addArrangedSubview(passwordTextField)
        stackView.addArrangedSubview(confirmPasswordTextField)
        stackView.addArrangedSubview(userTextField)
        stackView.addArrangedSubview(userTextField)
        return stackView
    }()
    private lazy var buttonCheckBox: UIButton = {
        var button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(ImageNameStruct.checkbox, for: .normal)
        button.setImage(ImageNameStruct.checkmark, for: .selected)
        button.addTarget(self, action: #selector(checkBoxClicked(_:)), for: .touchUpInside)
        return button
    }()
    private lazy var buttonSecurity: UIButton = {
        var button = UIButton()
        button.setImage(ImageNameStruct.eye, for: .selected)
        button.setImage(ImageNameStruct.crossedEye, for: .normal)
        button.contentMode = .center
        button.addTarget(self, action: #selector(eyeButtonTextField(_:)), for: .touchUpInside)
        return button
    }()
    private lazy var buttonConfirmSecurity: UIButton = {
        var button = UIButton()
        button.setImage(ImageNameStruct.eye, for: .selected)
        button.setImage(ImageNameStruct.crossedEye, for: .normal)
        button.contentMode = .center
        button.addTarget(self, action: #selector(eyeButtonTextField(_:)), for: .touchUpInside)
        return button
    }()
    private lazy var buttonSignUp: UIButton = {
        var button = UIButton()
        button.backgroundColor = ColorStruct.buttonSignIn
        button.setTitle(TextStruct.buttonSignIn, for: .normal)
        button.titleLabel?.font = FontSctuct.medium14
        button.setTitleColor(ColorStruct.forgotLabel, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.layer.cornerRadius = DimensionsStruct.cornerRadius
        button.addTarget(self, action: #selector(signUpClicked(_ :)), for: .touchUpInside)
        return button
    }()
    private lazy var stackVertical: UIStackView = {
        var stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .equalSpacing
        stackView.spacing = DimensionsStruct.spacingForButtonDontAccauntSignI
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.addArrangedSubview(buttonSignUp)
        stackView.addArrangedSubview(stackViewForButtonDontAccauntSignIn)
        return stackView
    }()
    private lazy var buttonDontAccaunt: UIButton = {
        var button = UIButton()
        button.setTitle(TextStruct.buttonDontAccaunt, for: .normal)
        button.setTitleColor(ColorStruct.textFieldPlaceholder,for: .normal)
        button.titleLabel?.font = FontSctuct.regular14
        return button
    }()
    private lazy var buttonDontAccauntSignIn: UIButton = {
        var button = UIButton()
        button.setTitle(TextStruct.buttonSignIn, for: .normal)
        button.setTitleColor(ColorStruct.forgotLabel,for: .normal)
        button.titleLabel?.font = FontSctuct.regular14
        button.addTarget(self, action: #selector(signInClicked( _:)), for: .touchUpInside)
        return button
    }()
    private lazy var stackViewForButtonDontAccauntSignIn: UIStackView = {
        var stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.distribution = .fill
        stackView.spacing = DimensionsStruct.spacingForButtonDontAccauntSignI
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.addArrangedSubview(buttonDontAccaunt)
        stackView.addArrangedSubview(buttonDontAccauntSignIn)
        return stackView
    }()
    private lazy var termsAndPrivacyLabel: UILabel = {
        var label = UILabel()
        label.textAlignment = .left
        label.backgroundColor = .clear
        label.textColor = ColorStruct.textForTextView
        let attributedString = NSMutableAttributedString(string: TextStruct.textView)
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.white , range: NSRange(location: 15, length: 13))
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.white , range: NSRange(location: 31, length: 15))
        label.attributedText = attributedString
        label.font = FontSctuct.regular14
        label.translatesAutoresizingMaskIntoConstraints = false
        label.isUserInteractionEnabled = true
        label.adjustsFontSizeToFitWidth = true
        label.addGestureRecognizer(UITapGestureRecognizer(target:self, action: #selector(tapLabel(gesture:))))
        return label
    }()
    //MARK: - Lifecycle functions
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = ColorStruct.viewBackground
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(hideKeyboardRecognizerClicked(_:)))
        view.addGestureRecognizer(tapRecognizer)
        addSubViewForView()
        emailTextField.delegate = self
        passwordTextField.delegate = self
        confirmPasswordTextField.delegate = self
        userTextField.delegate = self
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavigationBar()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        setupConstraintAgreeTermsFirst()
        setupConstraintForAvatarImageView()
        setupConstraintForButtonAdd()
        setupConstraintForStackViewForTextField()
        setupConstraintForStackVertical()
        setupConstraintForButtonDontSignIn()
        setupConstraintForCheckBox()
        avatarImageView.layer.cornerRadius = avatarImageView.frame.width / 2
    }
    //MARK: - IBAction
    @objc
    private func tapLabel(gesture: UITapGestureRecognizer) {
        let termsOfUse = (termsAndPrivacyLabel.text as! NSString).range(of: TextStruct.termsOfUseButton)
        let privacyPolicy = (termsAndPrivacyLabel.text as! NSString).range(of: TextStruct.privacyPolicyButton)
        if gesture.didTapAttributedTextInLabel(label: termsAndPrivacyLabel, inRange: termsOfUse) {
                let controller = TermsOfUseViewController()
            self.navigationController?.pushViewController(controller, animated: true)
        } else if gesture.didTapAttributedTextInLabel(label: termsAndPrivacyLabel, inRange: privacyPolicy) {
            let controller = PrivacyPolicyViewController()
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    @objc
    private func hideKeyboardRecognizerClicked(_ sender: UITapGestureRecognizer) {
        view.endEditing(true)
    }
    @objc
    private func signInClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @objc
    private func eyeButtonTextField(_ sender: Any){
        buttonSecurity.isSelected.toggle()
        passwordTextField.isSecureTextEntry.toggle()
    }
    @objc
    private func eyeButtonConfirmTextField(_ sender: Any){
        buttonConfirmSecurity.isSelected.toggle()
        confirmPasswordTextField.isSecureTextEntry.toggle()
    }
    @objc
    private func checkBoxClicked(_ sender: Any){
        buttonCheckBox.isSelected.toggle()
    }
    @objc
    private func signUpClicked(_ sender: Any){
        if buttonCheckBox.isSelected {
            self.navigationController?.navigationBar.backItem?.title = ""
            navigationController?.setNavigationBarHidden(false, animated: false)
            agreeTermsErrorView.isHidden = true
        } else {
            navigationController?.setNavigationBarHidden(true, animated: false)
            agreeTermsErrorView.isHidden = false
            return
        }
        var image = Data()
        guard let email = emailTextField.text,
              let password = passwordTextField.text,
              let confirmPassword = confirmPasswordTextField.text,
              password == confirmPassword else { return }
        Auth.auth().createUser(withEmail: email, password: password) { [weak self] user, error in
            guard let self = self,
                  let authData = user else { return }
            var dict: Dictionary <String, Any> = [
                FirebaseConstant.uid : authData.user.uid,
                FirebaseConstant.email : authData.user.email,
                FirebaseConstant.username  : self.userTextField.text,
                FirebaseConstant.profileImageUrl  : ""
            ]
            Database.database().reference().child(FirebaseConstant.users).child("\(authData.user.uid)").updateChildValues(dict) { (error, ref) in
            }
            let storageRef = Storage.storage().reference(forURL: FirebaseConstant.storageURL)
            let storageProfileRef = storageRef.child(FirebaseConstant.profilePhoto).child(authData.user.uid)
            let metadata = StorageMetadata()
            metadata.contentType = FirebaseConstant.profilePhotoContentType
            guard let imageData = self.avatarImageView.image?.jpegData(compressionQuality: 0.4) else { return }
            image = imageData
            storageProfileRef.putData(image, metadata: metadata) {[weak self] (storageMetaData, error) in
                guard let self = self,
                          error == nil else { return }
                storageProfileRef.downloadURL {  (url, error) in
                    if let metaImageUrl = url?.absoluteString{
                        print(metaImageUrl)
                        dict[FirebaseConstant.profileImageUrl] = metaImageUrl
                        
                        Database.database().reference().child(FirebaseConstant.users).child(authData.user.uid).updateChildValues(dict) { error, ref in
                            guard let error = error else { return }
                            self.showErrorAlert(error)
                        }
                    }
                }
            }
        }
    }
    //MARK: - Flow functions
    private func setupNavigationBar() {
        navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.backItem?.title = ""
        self.navigationController?.navigationBar.tintColor = .white
        self.navigationController?.navigationBar.barTintColor = ColorStruct.navigationBar
    }
    private func showErrorAlert(_ error: Error){
        let errorMessage = error.localizedDescription
        let alertVC = UIAlertController(title: nil, message: errorMessage, preferredStyle: .alert)
        let action = UIAlertAction(title: ButtonTitle.OK, style: .cancel, handler: nil)
        alertVC.addAction(action)
        self.present(alertVC, animated: true, completion: nil)
    }
    private func addSubViewForView() {
        view.addSubview(agreeTermsErrorView)
        agreeTermsErrorView.addSubview(agreeTermsFirst)
        view.addSubview(avatarImageView)
        view.addSubview(buttonAdd)
        view.addSubview(stackViewForTextField)
        view.addSubview(stackVertical)
        view.addSubview(stackViewForButtonDontAccauntSignIn)
        view.addSubview(buttonCheckBox)
        view.addSubview(termsAndPrivacyLabel)
    }
    private func setupConstraintAgreeTermsFirst(){
        agreeTermsErrorView.widthAnchor.constraint(equalToConstant: view.frame.width).isActive = true
        agreeTermsErrorView.heightAnchor.constraint(equalToConstant: 56).isActive = true
        agreeTermsErrorView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0).isActive = true
        agreeTermsErrorView.topAnchor.constraint(equalTo: view.topAnchor, constant: view.frame.height / 18.4).isActive = true
        agreeTermsErrorImageView.widthAnchor.constraint(equalToConstant: 24).isActive = true
        agreeTermsErrorImageView.heightAnchor.constraint(equalToConstant: 18).isActive = true
        agreeTermsFirst.topAnchor.constraint(equalTo: agreeTermsErrorView.topAnchor, constant: 15).isActive = true
        agreeTermsFirst.bottomAnchor.constraint(equalTo: agreeTermsErrorView.bottomAnchor, constant: -20).isActive = true
        agreeTermsFirst.leadingAnchor.constraint(equalTo: agreeTermsErrorView.leadingAnchor, constant: 30).isActive = true
        agreeTermsFirst.trailingAnchor.constraint(equalTo: agreeTermsErrorView.trailingAnchor, constant: -30).isActive = true
    }
    private func setupConstraintForAvatarImageView() {
        avatarImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        avatarImageView.topAnchor.constraint(equalTo: view.topAnchor, constant: view.frame.height / 5.9).isActive = true
        avatarImageView.heightAnchor.constraint(equalToConstant: view.frame.height / 8.53).isActive = true
        avatarImageView.widthAnchor.constraint(equalToConstant: view.frame.height / 8.53).isActive = true
        avatarImageView.layer.cornerRadius = avatarImageView.frame.width / 2
    }
    private func setupConstraintForButtonAdd() {
        buttonAdd.trailingAnchor.constraint(equalTo: avatarImageView.trailingAnchor, constant: view.frame.height / 281.3).isActive = true
        buttonAdd.bottomAnchor.constraint(equalTo: avatarImageView.bottomAnchor, constant: view.frame.height / 281.3).isActive = true
        buttonAdd.leadingAnchor.constraint(equalTo: avatarImageView.leadingAnchor, constant: view.frame.height / 11.88).isActive = true
        buttonAdd.topAnchor.constraint(equalTo: avatarImageView.topAnchor, constant: view.frame.height / 11.88).isActive = true
    }
    private func setupConstraintForStackViewForTextField() {
        stackViewForTextField.topAnchor.constraint(equalTo: avatarImageView.bottomAnchor, constant: view.frame.height / 8.4).isActive = true
        stackViewForTextField.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: view.frame.height / 42.2).isActive = true
        stackViewForTextField.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: view.frame.height / -42.2).isActive = true
        emailTextField.heightAnchor.constraint(equalToConstant: view.frame.height / 21.1).isActive = true
    }
    private func setupConstraintForCheckBox() {
        buttonCheckBox.widthAnchor.constraint(equalToConstant: view.frame.height / 42.2).isActive = true
        buttonCheckBox.heightAnchor.constraint(equalToConstant: view.frame.height / 42.2).isActive = true
        buttonCheckBox.topAnchor.constraint(equalTo: stackViewForTextField.bottomAnchor, constant: view.frame.height / 35.76).isActive = true
        buttonCheckBox.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: view.frame.height / 35.16).isActive = true
        termsAndPrivacyLabel.topAnchor.constraint(equalTo: stackViewForTextField.bottomAnchor, constant: view.frame.height / 33.76).isActive = true
        termsAndPrivacyLabel.heightAnchor.constraint(equalToConstant: view.frame.height / 46.88).isActive = true
        termsAndPrivacyLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: view.frame.height / -17.583).isActive = true
        termsAndPrivacyLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: view.frame.height / 16.23).isActive = true
    }
    private func setupConstraintForStackVertical() {
        stackVertical.topAnchor.constraint(equalTo: termsAndPrivacyLabel.bottomAnchor,constant: view.frame.height / 14.305).isActive = true
        stackVertical.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: view.frame.height / 35.16).isActive = true
        stackVertical.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: view.frame.height / -35.16).isActive = true
        buttonSignUp.heightAnchor.constraint(equalToConstant: view.frame.height / 18.347).isActive = true
    }
    private func setupConstraintForButtonDontSignIn() {
        stackViewForButtonDontAccauntSignIn.topAnchor.constraint(equalTo: stackVertical.bottomAnchor, constant: view.frame.height / 52.75).isActive = true
        stackViewForButtonDontAccauntSignIn.centerXAnchor.constraint(equalTo: stackVertical.centerXAnchor).isActive = true
    }
}
//MARK: - PickerPresenter
extension SignUpViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let imageSelected = info[UIImagePickerController.InfoKey.editedImage] as? UIImage{
            avatarImageView.image = imageSelected
        }
        if let imageOriginal = info[UIImagePickerController.InfoKey.originalImage] as? UIImage{
            avatarImageView.image = imageOriginal
        }
        buttonAdd.isSelected = true
        picker.dismiss(animated: true, completion: nil)
    }
    @objc
    func presentPicker(_ sender: Any){
        let picker = UIImagePickerController()
        UINavigationBar.appearance().tintColor = ColorStruct.viewBackground
        picker.sourceType = .photoLibrary
        picker.allowsEditing = true
        picker.delegate = self
        picker.modalPresentationStyle = .fullScreen
        self.present(picker, animated: true, completion: nil)
    }
}
//MARK: - UITextFieldDelegate
extension SignUpViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        switch textField.tag {
        case 0:
            textField.leftView(ImageNameStruct.mailBlack, imageWidth: DimensionsStruct.imageWidth, padding: DimensionsStruct.padding)
            textField.textColor =  ColorStruct.textFieldColor
            textField.backgroundColor = ColorStruct.textFieldPlaceholder
            textField.attributedPlaceholder = NSAttributedString(string: TextStruct.emailTextField,
                                                                         attributes: [NSAttributedString.Key.foregroundColor: ColorStruct.textFieldColor])
        case 1:
            textField.leftView(ImageNameStruct.lockBlack, imageWidth: DimensionsStruct.imageWidth, padding: DimensionsStruct.padding)
            buttonSecurity.setImage(ImageNameStruct.eyeBlack, for: .selected)
            buttonSecurity.setImage(ImageNameStruct.crossedEyeBlack, for: .normal)
            textField.textColor =  ColorStruct.textFieldColor
            textField.backgroundColor = ColorStruct.textFieldPlaceholder
            textField.attributedPlaceholder = NSAttributedString(string: TextStruct.passwordTextField,
                                                                         attributes: [NSAttributedString.Key.foregroundColor: ColorStruct.textFieldColor])
        case 2 :
            textField.leftView(ImageNameStruct.lockBlack, imageWidth: DimensionsStruct.imageWidth, padding: DimensionsStruct.padding)
            buttonConfirmSecurity.setImage(ImageNameStruct.eyeBlack, for: .selected)
            buttonConfirmSecurity.setImage(ImageNameStruct.crossedEyeBlack, for: .normal)
            textField.textColor =  ColorStruct.textFieldColor
            textField.backgroundColor = ColorStruct.textFieldPlaceholder
            textField.attributedPlaceholder = NSAttributedString(string: TextStruct.confirmPassword,
                                                                         attributes: [NSAttributedString.Key.foregroundColor: ColorStruct.textFieldColor])
        case 3:
            textField.leftViewMode = .always
            textField.leftView(ImageNameStruct.profileBlack, imageWidth: DimensionsStruct.imageWidth, padding: DimensionsStruct.padding)
            textField.textColor =  ColorStruct.textFieldColor
            textField.backgroundColor = ColorStruct.textFieldPlaceholder
            textField.attributedPlaceholder = NSAttributedString(string: TextStruct.userTextField,
                                                                         attributes: [NSAttributedString.Key.foregroundColor: ColorStruct.textFieldColor])
        default:
            break
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField.tag {
        case 0:
            textField.leftView(ImageNameStruct.mail, imageWidth: DimensionsStruct.imageWidth, padding: DimensionsStruct.padding)
            textField.textColor =  ColorStruct.textFieldPlaceholder
            textField.backgroundColor = ColorStruct.textFieldColor
            textField.attributedPlaceholder = NSAttributedString(string: TextStruct.emailTextField,
                                                                         attributes: [NSAttributedString.Key.foregroundColor: ColorStruct.textFieldPlaceholder])
        case 1:
            buttonSecurity.setImage(ImageNameStruct.eye, for: .selected)
            buttonSecurity.setImage(ImageNameStruct.crossedEye, for: .normal)
            textField.leftView(ImageNameStruct.lock, imageWidth: DimensionsStruct.imageWidth, padding: DimensionsStruct.padding)
            textField.textColor =  ColorStruct.textFieldPlaceholder
            textField.backgroundColor = ColorStruct.textFieldColor
            textField.attributedPlaceholder = NSAttributedString(string: TextStruct.passwordTextField,
                                                                         attributes: [NSAttributedString.Key.foregroundColor: ColorStruct.textFieldPlaceholder])
        case 2:
            buttonConfirmSecurity.setImage(ImageNameStruct.eye, for: .selected)
            buttonConfirmSecurity.setImage(ImageNameStruct.crossedEye, for: .normal)
            textField.leftView(ImageNameStruct.lock, imageWidth: DimensionsStruct.imageWidth, padding: DimensionsStruct.padding)
            textField.textColor =  ColorStruct.textFieldPlaceholder
            textField.backgroundColor = ColorStruct.textFieldColor
            textField.attributedPlaceholder = NSAttributedString(string: TextStruct.confirmPassword,
                                                                         attributes: [NSAttributedString.Key.foregroundColor: ColorStruct.textFieldPlaceholder])
        case 3:
            textField.leftView(ImageNameStruct.profile, imageWidth: DimensionsStruct.imageWidth, padding: DimensionsStruct.padding)
            textField.textColor =  ColorStruct.textFieldPlaceholder
            textField.backgroundColor = ColorStruct.textFieldColor
            textField.attributedPlaceholder = NSAttributedString(string: TextStruct.userTextField,
                                                                         attributes: [NSAttributedString.Key.foregroundColor: ColorStruct.textFieldPlaceholder])
        default:
            break
        }
    }
}
