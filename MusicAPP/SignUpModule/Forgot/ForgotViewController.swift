//
//  ForgotViewController.swift
//  music
//
//  Created by Николай on 25.10.21.
//

import UIKit
import FirebaseAuth

class ForgotViewController: UIViewController {
    //MARK: - UI elements in code
    private lazy var firstLabel: UILabel = {
        var label = UILabel()
        label.text = TextStruct.forgotMusificationPassword
        label.font = FontSctuct.bold18
        label.textColor = ColorStruct.forgotLabel
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    private lazy var secondLabel: UILabel = {
        var label = UILabel()
        label.text = TextStruct.forgotPasswordDescription
        label.font = FontSctuct.regular14
        label.textColor = ColorStruct.textFieldPlaceholder
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        return label
    }()
    private lazy var userTextField: UITextField = {
        var userTextField = UITextField()
        userTextField.leftView(ImageNameStruct.profile, imageWidth: DimensionsStruct.imageWidth, padding: DimensionsStruct.padding)
        userTextField.backgroundColor = ColorStruct.textFieldColor
        userTextField.translatesAutoresizingMaskIntoConstraints = false
        userTextField.layer.cornerRadius = DimensionsStruct.cornerRadius
        userTextField.font = FontSctuct.regular14
        userTextField.attributedPlaceholder = NSAttributedString(string: TextStruct.emailOrUsername,
                                                                 attributes: [NSAttributedString.Key.foregroundColor: ColorStruct.textFieldPlaceholder])
        return userTextField
    }()
    private  lazy var buttonSendReset: UIButton = {
        var button = UIButton()
        button.backgroundColor = ColorStruct.buttonSignIn
        button.setTitle(ButtonTitle.forgotPassword, for: .normal)
        button.titleLabel?.font = FontSctuct.medium14
        button.setTitleColor(ColorStruct.forgotLabel, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.layer.cornerRadius = DimensionsStruct.cornerRadius
        button.addTarget(self, action: #selector(resetPin(_:)), for: .touchUpInside)
        return button
    }()
    //MARK: - Lifecycle functions
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = ColorStruct.viewBackground
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(hideKeyboardRecognizerClicked(_:)))
        view.addGestureRecognizer(tapRecognizer)
        addSubViewForView()
        userTextField.delegate = self
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavigationBar()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        setupConstraint()
    }
    private func addSubViewForView() {
        view.addSubview(firstLabel)
        view.addSubview(secondLabel)
        view.addSubview(userTextField)
        view.addSubview(buttonSendReset)
    }
    //MARK: - IBAction
    @objc
    private func hideKeyboardRecognizerClicked(_ sender: UITapGestureRecognizer) {
        view.endEditing(true)
    }
    @objc
    private func resetPin(_ sender: Any){
        Auth.auth().sendPasswordReset(withEmail: userTextField.text ?? "") { error in
            if let error = error {
                print(error.localizedDescription)
                return
            }
            self.navigationController?.pushViewController(SignInViewController(), animated: true)
        }
    }
    private func setupNavigationBar() {
        navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.backItem?.title = ""
        self.navigationController?.navigationBar.tintColor = ColorStruct.forgotLabel
        title = TextStruct.resetPasswordVCTitle
    }
    //MARK: - setup Constraint
    private func setupConstraint() {
        firstLabel.topAnchor.constraint(equalTo: view.topAnchor, constant: view.frame.height / 7.54).isActive = true
        firstLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: view.frame.height / 35.17).isActive = true
        firstLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: view.frame.height / -16.23).isActive = true
        secondLabel.topAnchor.constraint(equalTo: firstLabel.bottomAnchor, constant: view.frame.height / 52.75).isActive = true
        secondLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: view.frame.height / 35.17).isActive = true
        secondLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: view.frame.height / -35.17).isActive = true
        userTextField.topAnchor.constraint(equalTo: secondLabel.bottomAnchor, constant: view.frame.height / 35.17).isActive = true
        userTextField.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: view.frame.height / 36.7).isActive = true
        userTextField.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: view.frame.height / -33.76).isActive = true
        userTextField.heightAnchor.constraint(equalToConstant: view.frame.height / 21.1).isActive = true
        buttonSendReset.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: view.frame.height / -14.56).isActive = true
        buttonSendReset.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: view.frame.height / 35.17).isActive = true
        buttonSendReset.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: view.frame.height / -35.17).isActive = true
        buttonSendReset.heightAnchor.constraint(equalToConstant: view.frame.height / 18.35).isActive = true
    }
}
//MARK: - UITextFieldDelegate
extension ForgotViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        switch textField.tag {
        case 0:
            textField.leftViewMode = .always
            textField.leftView(ImageNameStruct.profileBlack, imageWidth: DimensionsStruct.imageWidth, padding: DimensionsStruct.padding)
            textField.textColor =  ColorStruct.textFieldColor
            textField.backgroundColor = ColorStruct.textFieldPlaceholder
            textField.attributedPlaceholder = NSAttributedString(string: TextStruct.emailOrUsername,
                                                                         attributes: [NSAttributedString.Key.foregroundColor: ColorStruct.textFieldColor])
        default:
            break
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField.tag {
        case 0:
            textField.leftView(ImageNameStruct.profile, imageWidth: DimensionsStruct.imageWidth, padding: DimensionsStruct.padding)
            textField.textColor =  ColorStruct.textFieldPlaceholder
            textField.backgroundColor = ColorStruct.textFieldColor
            textField.attributedPlaceholder = NSAttributedString(string: TextStruct.emailOrUsername,
                                                                     attributes: [NSAttributedString.Key.foregroundColor: ColorStruct.textFieldPlaceholder])
        default:
            break
        }
    }
}
