//
//  LoginViewController.swift
//  music
//
//  Created by Николай on 13.10.21.
//

import UIKit
import Firebase
import FirebaseDatabase
import FirebaseAuth
import GoogleSignIn

class SignInViewController: UIViewController {
    //MARK: - UI elements in code
    private lazy var logoImageView: UIImageView = {
        var logoImageView = UIImageView()
        logoImageView.image = ImageNameStruct.logo
        logoImageView.translatesAutoresizingMaskIntoConstraints = false
        return logoImageView
    }()
    private lazy var forgotPasswordButton: UIButton = {
        var forgotLabel = UIButton()
        forgotLabel.translatesAutoresizingMaskIntoConstraints = false
        forgotLabel.setTitle(TextStruct.forgotText, for: .normal)
        forgotLabel.setTitleColor(ColorStruct.forgotLabel, for: .normal)
        forgotLabel.contentHorizontalAlignment = .left
        forgotLabel.titleLabel?.font = FontSctuct.regular14
        forgotLabel.addTarget(self, action: #selector(forgotPasswordClicked(_:)), for: .touchUpInside)
        return forgotLabel
    }()
    private lazy var emailTextField: UITextField = {
        var emailTextField = UITextField()
        emailTextField.leftViewMode = .always
        emailTextField.leftView(ImageNameStruct.profile, imageWidth: DimensionsStruct.imageWidth, padding: DimensionsStruct.padding)
        emailTextField.backgroundColor = ColorStruct.textFieldColor
        emailTextField.translatesAutoresizingMaskIntoConstraints = false
        emailTextField.layer.cornerRadius = DimensionsStruct.cornerRadius
        emailTextField.font = FontSctuct.regular14
        emailTextField.attributedPlaceholder = NSAttributedString(string: TextStruct.emailTextFieldPlaceholder,
                                                                  attributes: [NSAttributedString.Key.foregroundColor: ColorStruct.textFieldPlaceholder])
        emailTextField.tag = 0
        return emailTextField
    }()
    private lazy var passwordTextField: UITextField = {
        var passwordTextField = UITextField()
        passwordTextField.leftView(ImageNameStruct.lock, imageWidth: DimensionsStruct.imageWidth, padding: DimensionsStruct.padding)
        passwordTextField.translatesAutoresizingMaskIntoConstraints = false
        passwordTextField.layer.masksToBounds = true
        passwordTextField.rightView = buttonSecurity
        passwordTextField.tag = 1
        passwordTextField.isSecureTextEntry = true
        passwordTextField.rightViewMode = .always
        passwordTextField.rightView?.widthAnchor.constraint(equalToConstant: view.frame.height / 18.76).isActive = true
        passwordTextField.backgroundColor = ColorStruct.textFieldColor
        passwordTextField.translatesAutoresizingMaskIntoConstraints = false
        passwordTextField.layer.cornerRadius = DimensionsStruct.cornerRadius
        passwordTextField.font = FontSctuct.regular14
        passwordTextField.attributedPlaceholder = NSAttributedString(string: TextStruct.passwordTextField,
                                                                     attributes: [NSAttributedString.Key.foregroundColor: ColorStruct.textFieldPlaceholder])
        return passwordTextField
    }()
    private lazy var buttonSecurity: UIButton = {
        var buttonSecurity = UIButton()
        buttonSecurity.setImage(ImageNameStruct.eye, for: .selected)
        buttonSecurity.setImage(ImageNameStruct.crossedEye, for: .normal)
        buttonSecurity.contentMode = .center
        buttonSecurity.addTarget(self, action: #selector(toggleEyePasswordTextFieldClicked(_:)), for: .touchUpInside)
        return buttonSecurity
    }()
    private lazy var imageViewForEmailTF: UIImageView = {
        var imageViewForEmailTF = UIImageView()
        imageViewForEmailTF.image = ImageNameStruct.profile
        return imageViewForEmailTF
    }()
    private lazy var stackViewForTextField: UIStackView = {
        var stackViewForTextField = UIStackView()
        stackViewForTextField.axis = .vertical
        stackViewForTextField.distribution = .fillEqually
        stackViewForTextField.spacing = DimensionsStruct.spacingForTextField
        stackViewForTextField.translatesAutoresizingMaskIntoConstraints = false
        stackViewForTextField.addArrangedSubview(emailTextField)
        stackViewForTextField.addArrangedSubview(passwordTextField)
        stackViewForTextField.addArrangedSubview(forgotPasswordButton)
        return stackViewForTextField
    }()
    private lazy var buttonSignIn: UIButton = {
        var buttonSignIn = UIButton()
        buttonSignIn.backgroundColor = ColorStruct.buttonSignIn
        buttonSignIn.setTitle(TextStruct.buttonSignIn, for: .normal)
        buttonSignIn.titleLabel?.font = FontSctuct.medium14
        buttonSignIn.setTitleColor(ColorStruct.forgotLabel, for: .normal)
        buttonSignIn.translatesAutoresizingMaskIntoConstraints = false
        buttonSignIn.layer.cornerRadius = DimensionsStruct.cornerRadius
        buttonSignIn.addTarget(self, action: #selector(signInClicked( _:)), for: .touchUpInside)
        return buttonSignIn
    }()
    private lazy var lineImageView: UIImageView = {
        var lineImageView = UIImageView()
        lineImageView.image = ImageNameStruct.line
        lineImageView.contentMode = .scaleAspectFit
        return lineImageView
    }()
    private lazy var buttonSignInWithGoogle: UIButton = {
        var buttonSignInWithGoogle = UIButton()
        buttonSignInWithGoogle.backgroundColor = ColorStruct.forgotLabel
        buttonSignInWithGoogle.setTitle(TextStruct.buttonSignInWithGoogle, for: .normal)
        buttonSignInWithGoogle.setTitleColor(ColorStruct.textFieldColor,for: .normal)
        buttonSignInWithGoogle.titleLabel?.font = FontSctuct.medium14
        buttonSignInWithGoogle.setImage(ImageNameStruct.google, for: .normal)
        buttonSignInWithGoogle.layer.cornerRadius = DimensionsStruct.cornerRadius
        buttonSignInWithGoogle.addTarget(self, action: #selector(signInWithGoogleClicked(_:)), for: .touchUpInside)
        return buttonSignInWithGoogle
    }()
    private lazy var stackViewForButton: UIStackView = {
        var stackViewForButton = UIStackView()
        stackViewForButton.axis = .vertical
        stackViewForButton.distribution = .fillEqually
        stackViewForButton.spacing = DimensionsStruct.spacingForButton
        stackViewForButton.translatesAutoresizingMaskIntoConstraints = false
        stackViewForButton.addArrangedSubview(buttonSignIn)
        stackViewForButton.addArrangedSubview(lineImageView)
        stackViewForButton.addArrangedSubview(buttonSignInWithGoogle)
        stackViewForButton.addArrangedSubview(buttonDontAccaunt)
        return stackViewForButton
    }()
    private lazy var buttonDontAccaunt: UIButton = {
        var buttonDontAccaunt = UIButton()
        buttonDontAccaunt.setTitle(TextStruct.buttonDontAccaunt, for: .normal)
        buttonDontAccaunt.setTitleColor(ColorStruct.textFieldPlaceholder,for: .normal)
        buttonDontAccaunt.titleLabel?.font = FontSctuct.regular14
        return buttonDontAccaunt
    }()
    private lazy var buttonDontAccauntSignIn: UIButton = {
        var buttonDontAccauntSignIn = UIButton()
        buttonDontAccauntSignIn.setTitle(TextStruct.buttonDontAccauntSignIn, for: .normal)
        buttonDontAccauntSignIn.setTitleColor(ColorStruct.forgotLabel,for: .normal)
        buttonDontAccauntSignIn.titleLabel?.font = FontSctuct.regular14
        buttonDontAccauntSignIn.addTarget(self, action: #selector(signUpClicked(_:)), for: .touchUpInside)
        return buttonDontAccauntSignIn
    }()
    private lazy var stackViewForButtonDontAccauntSignIn: UIStackView = {
        var stackViewForButtonDontAccauntSignIn = UIStackView()
        stackViewForButtonDontAccauntSignIn.axis = .horizontal
        stackViewForButtonDontAccauntSignIn.distribution = .equalSpacing
        stackViewForButtonDontAccauntSignIn.spacing = DimensionsStruct.spacingForButtonDontAccauntSignI
        stackViewForButtonDontAccauntSignIn.translatesAutoresizingMaskIntoConstraints = false
        stackViewForButtonDontAccauntSignIn.addArrangedSubview(buttonDontAccaunt)
        stackViewForButtonDontAccauntSignIn.addArrangedSubview(buttonDontAccauntSignIn)
        return stackViewForButtonDontAccauntSignIn
    }()
    //MARK: - Lifecycle functions
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        view.backgroundColor = ColorStruct.viewBackground
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(hideKeyboardRecognizerClicked(_:)))
        view.addGestureRecognizer(tapRecognizer)
        emailTextField.delegate = self
        passwordTextField.delegate = self
        addSubViewForView()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavigationBar()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        setupConstraintForIogoImageView()
        setupConstraintForStackViewTextField()
        setupConstraintForTextField()
        setupConstraintForStackViewForButton()
        setupConstraintForButtonSignIn()
        setupConstraintForButtonDontSignIn()
    }
    //MARK: - IBAction
    @objc
    private func hideKeyboardRecognizerClicked(_ sender: UITapGestureRecognizer) {
        view.endEditing(true)
    }
    @objc
    private func forgotPasswordClicked(_ sender: UIButton) {
        let forgotPasswordVC = ForgotViewController()
        self.navigationController?.pushViewController(forgotPasswordVC, animated: true)
    }
    @objc
    private func signInClicked(_ sender: Any){
        if let email = emailTextField.text, let password = passwordTextField.text{
            Auth.auth().signIn(withEmail: email, password: password) { (user, error) in
                guard let user = user else { return self.showErrorAlert(error!) }
                self.navigationController?.navigationBar.isHidden = false
                self.navigationController?.pushViewController(TabBarViewController(), animated: true)
            }
        }
    }
    @objc
    private func toggleEyePasswordTextFieldClicked(_ sender: Any){
        buttonSecurity.isSelected.toggle()
        passwordTextField.isSecureTextEntry.toggle()
    }
    @objc
    private func signUpClicked(_ sender: Any) {
        let signUpViewController = SignUpViewController()
        self.navigationController?.pushViewController(signUpViewController, animated: true)
    }
    @objc
    private func signInWithGoogleClicked(_ sender: UIButton){
        guard let clientID = FirebaseApp.app()?.options.clientID else { return }
        let config = GIDConfiguration(clientID: clientID)
        GIDSignIn.sharedInstance.signIn(with: config, presenting: self) { [weak self] user, error in
            if let error = error { return }
            guard let authentication = user?.authentication,
                  let idToken = authentication.idToken else { return }
            let credential = GoogleAuthProvider.credential(withIDToken: idToken, accessToken: authentication.accessToken)
            Auth.auth().signIn(with: credential) { authResult, error in
                guard let user = user else { return }
                guard let email = user.profile?.email,
                      let uuid = user.userID,
                      let fullName = user.profile?.name,
                      let profilePicUrl = user.profile?.imageURL(withDimension: 320) else { return }
                let googleVC = SignUpViaGoogleViewController()
                googleVC.email = email
                googleVC.uuid = uuid
                googleVC.profilePhoto = profilePicUrl
                self?.navigationController?.isNavigationBarHidden = false
                self?.navigationController?.pushViewController(googleVC, animated: true)
            }
        }
    }
    //MARK: - Flow function
    private func setupNavigationBar() {
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
    private func addSubViewForView() {
        view.addSubview(logoImageView)
        view.addSubview(stackViewForTextField)
        view.addSubview(stackViewForButton)
        view.addSubview(stackViewForButtonDontAccauntSignIn)
    }
    private func showErrorAlert(_ error: Error){
        let errorMessage = error.localizedDescription
        let alertVC = UIAlertController(title: nil, message: errorMessage, preferredStyle: .alert)
        let action = UIAlertAction(title: ButtonTitle.OK, style: .cancel, handler: nil)
        alertVC.addAction(action)
        self.present(alertVC, animated: true, completion: nil)
    }
    private func setupConstraintForIogoImageView() {
        logoImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        logoImageView.topAnchor.constraint(equalTo: view.topAnchor, constant: view.frame.height / 9).isActive = true
        logoImageView.heightAnchor.constraint(equalToConstant: view.frame.height / 4.24).isActive = true
        logoImageView.widthAnchor.constraint(equalToConstant: view.frame.height / 4.24).isActive = true
    }
    private func setupConstraintForStackViewTextField() {
        stackViewForTextField.centerXAnchor.constraint(equalTo: logoImageView.centerXAnchor).isActive = true
        stackViewForTextField.topAnchor.constraint(equalTo: logoImageView.bottomAnchor, constant: view.frame.height / 17.583).isActive = true
        stackViewForTextField.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 23).isActive = true
        stackViewForTextField.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -26).isActive = true
    }
    private func setupConstraintForTextField() {
        emailTextField.heightAnchor.constraint(equalToConstant: view.frame.height / 21.1).isActive = true
    }
    private func setupConstraintForStackViewForButton() {
        stackViewForButton.centerXAnchor.constraint(equalTo: stackViewForTextField.centerXAnchor).isActive = true
        stackViewForButton.topAnchor.constraint(equalTo: logoImageView.bottomAnchor, constant: view.frame.height / 2.87).isActive = true
        stackViewForButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 23).isActive = true
        stackViewForButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -26).isActive = true
    }
    private func setupConstraintForButtonSignIn() {
        buttonSignIn.heightAnchor.constraint(equalToConstant: view.frame.height / 18.4).isActive = true
        buttonSignInWithGoogle.heightAnchor.constraint(equalToConstant: view.frame.height / 21.1).isActive = true
    }
    private func setupConstraintForButtonDontSignIn() {
        stackViewForButtonDontAccauntSignIn.topAnchor.constraint(equalTo: stackViewForButton.bottomAnchor, constant: 16).isActive = true
        stackViewForButtonDontAccauntSignIn.centerXAnchor.constraint(equalTo: stackViewForButton.centerXAnchor).isActive = true
    }
}
//MARK: - UITextFieldDelegate
extension SignInViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        //        let set = NSCharacterSet(charactersIn: "~`!#$%^&*()+=-/;:\"\'{}[]<>^?,")
        //          let inverted = set.inverted;
        //
        //          let filtered = string.components(separatedBy: inverted).joined(separator: "")
        //          return filtered != string;
        let currentText = emailTextField.text ?? ""
        guard let stringRange = Range(range, in: currentText) else { return false }
        
        let updateText = currentText.replacingCharacters(in: stringRange, with: string)
        
        return updateText.count < 320
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        switch textField.tag {
        case 0:
            textField.leftViewMode = .always
            textField.leftView(ImageNameStruct.profileBlack, imageWidth: DimensionsStruct.imageWidth, padding: DimensionsStruct.padding)
            textField.textColor =  ColorStruct.textFieldColor
            textField.backgroundColor = ColorStruct.textFieldPlaceholder
            textField.attributedPlaceholder = NSAttributedString(string: TextStruct.emailOrUsername,
                                                                         attributes: [NSAttributedString.Key.foregroundColor: ColorStruct.textFieldColor])
        case 1:
            textField.leftView(ImageNameStruct.lockBlack, imageWidth: DimensionsStruct.imageWidth, padding: DimensionsStruct.padding)
            buttonSecurity.setImage(ImageNameStruct.eyeBlack, for: .selected)
            buttonSecurity.setImage(ImageNameStruct.crossedEyeBlack, for: .normal)
            textField.textColor =  ColorStruct.textFieldColor
            textField.backgroundColor = ColorStruct.textFieldPlaceholder
            textField.attributedPlaceholder = NSAttributedString(string: TextStruct.passwordTextField,
                                                                         attributes: [NSAttributedString.Key.foregroundColor: ColorStruct.textFieldColor])
        default:
            break
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField.tag {
        case 0:
            textField.leftView(ImageNameStruct.profile, imageWidth: DimensionsStruct.imageWidth, padding: DimensionsStruct.padding)
            textField.textColor =  ColorStruct.textFieldPlaceholder
            textField.backgroundColor = ColorStruct.textFieldColor
            textField.attributedPlaceholder = NSAttributedString(string: TextStruct.emailOrUsername,
                                                                         attributes: [NSAttributedString.Key.foregroundColor: ColorStruct.textFieldPlaceholder])
        case 1:
            buttonSecurity.setImage(ImageNameStruct.eye, for: .selected)
            buttonSecurity.setImage(ImageNameStruct.crossedEye, for: .normal)
            textField.leftView(ImageNameStruct.lock, imageWidth: DimensionsStruct.imageWidth, padding: DimensionsStruct.padding)
            textField.textColor =  ColorStruct.textFieldPlaceholder
            textField.backgroundColor = ColorStruct.textFieldColor
            textField.attributedPlaceholder = NSAttributedString(string: TextStruct.passwordTextField,
                                                                         attributes: [NSAttributedString.Key.foregroundColor: ColorStruct.textFieldPlaceholder])
        default:
            break
        }
    }
}

