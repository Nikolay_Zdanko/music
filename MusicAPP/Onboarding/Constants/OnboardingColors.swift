//
//  BackgroundColor.swift
//  MusicAPP
//
//  Created by Егор Евсеенко on 19.10.2021.
//

import UIKit

struct OnboardingColors{
    static let onboarding = UIColor(red: 0.176, green: 0.122, blue: 0.208, alpha: 1)
    static let descriptionText = UIColor(red: 0.929, green: 0.949, blue: 0.957, alpha: 1)
    static let nextButton = UIColor(red: 0.851, green: 0.016, blue: 0.161, alpha: 1)
    static let skipButton = UIColor(red: 0.929, green: 0.949, blue: 0.957, alpha: 1)
}
