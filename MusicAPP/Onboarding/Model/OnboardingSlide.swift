//
//  OnboardingSlide.swift
//  MusicAPP
//
//  Created by Егор Евсеенко on 13.10.2021.
//

import UIKit

struct OnboardingSlide{
    let description: String
    let image: String
    static var descriptions: [String] = ["Music collected especially for you",
                                         "Sound \nthat pleses",
                                         "Listen on any device even without internet",
                                         "Music collected especially for you"]
    static var images: [String] = ["OnboardingImage_1",
                                   "OnboardingImage_2",
                                   "OnboardingImage_3"]
}
