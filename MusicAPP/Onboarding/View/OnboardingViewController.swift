import UIKit

class OnboardingViewController: UIPageViewController{
    //MARK: - Variable
    private var slide: [OnboardingSlide] = []
    private let scrollView = UIScrollView()
    private lazy var pageControl: UIPageControl = {
        var pageControl = UIPageControl()
        pageControl.numberOfPages = 3
        pageControl.backgroundColor = .clear
        return pageControl
    }()
    private lazy var nextButton: UIButton = {
        let nextButton = UIButton()
        nextButton.translatesAutoresizingMaskIntoConstraints = false
        nextButton.setTitle(ButtonTitle.next, for: .normal)
        nextButton.backgroundColor = OnboardingColors.nextButton
        nextButton.layer.cornerRadius = 21
        nextButton.titleLabel?.font = FontSctuct.normal15
        nextButton.addTarget(self, action: #selector(nextButtonClicked), for: .touchUpInside)
        return nextButton
    }()
    private lazy var skipButton: UIButton = {
        let skipButton = UIButton()
        skipButton.setTitle(ButtonTitle.skip, for: .normal)
        skipButton.tintColor = OnboardingColors.skipButton
        skipButton.titleLabel?.font = FontSctuct.normal11
        skipButton.addTarget(self, action: #selector(skipButtonClicked(_:)), for: .touchUpInside)
        return skipButton
    }()
    private lazy var slideImage = UIImageView()
    private lazy var slideDescription = UILabel()
    private lazy var descriptionStackView = UIStackView()
    //MARK: - Lifecycle functions
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        let navBar = UINavigationBar(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 44))
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: skipButton)
        view.backgroundColor = OnboardingColors.onboarding
        scrollView.delegate = self
        view.addSubview(navBar)
        view.addSubview(scrollView)
        view.addSubview(pageControl)
        setUpNextButtonConstraints()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        pageControl.frame = CGRect(x: 0,
                                   y: nextButton.frame.origin.y - 100,
                                   width: view.frame.size.width ,
                                   height: 70)
        scrollView.frame = CGRect(x: 0,
                                  y: 0,
                                  width: view.frame.size.width ,
                                  height: view.frame.size.height)
        if scrollView.subviews.count == pageControl.numberOfPages - 1{
            configureScrollView()
        }
    }
    //MARK: - Image and Description functions
    private func slideImage(_ name: String) -> UIImageView{
        let imageView:UIImageView = UIImageView()
        imageView.image = UIImage(named: name)
        imageView.image?.withTintColor(.systemYellow)
        imageView.contentMode = .scaleAspectFit
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }
    private func slideDescription(_ text: String) -> UILabel {
        let label = UILabel()
        label.text = text
        label.translatesAutoresizingMaskIntoConstraints = false
        label.adjustsFontSizeToFitWidth = true
        label.numberOfLines = 0
        label.textAlignment = .center
        label.textColor = OnboardingColors.descriptionText
        label.font = FontSctuct.bold35
        return label
    }
    //MARK: - IBAction
    @objc
    private func skipButtonClicked(_ sender: UIButton){
        let signInVC = SignInViewController()
        self.navigationController?.pushViewController(signInVC, animated: true)
    }
    @objc
    private func nextButtonClicked(_ sender: UIPageControl){
        if pageControl.currentPage == pageControl.numberOfPages - 1{
            let signInVC = SignInViewController()
            self.navigationController?.pushViewController(signInVC, animated: true)
        }
        scrollView.frame.origin.x += view.frame.size.width
        print(scrollView.frame.origin.x)
        UIView.animate(withDuration: 2) {
            self.scrollView.contentOffset.x += self.view.frame.width
        }
    }
    //MARK: - Flow Function
    private func configureScrollView(){
        scrollView.contentSize = CGSize(width: view.frame.width * CGFloat(pageControl.numberOfPages),
                                        height: scrollView.frame.size.height)
        scrollView.isPagingEnabled = true
        scrollView.showsHorizontalScrollIndicator = false
        for x in 0..<pageControl.numberOfPages{
            let page = UIView(frame: CGRect(x: CGFloat(x) * view.frame.size.width,
                                            y: 0,
                                            width: scrollView.frame.size.width,
                                            height: scrollView.frame.size.height))
            slide.append(OnboardingSlide(description: OnboardingSlide.descriptions[x], image: OnboardingSlide.images[x]))
            configureViewForSlide(page: page, index: x)
        }
    }
    private func configureViewForSlide(page: UIView, index: Int){
        scrollView.addSubview(page)
        slideImage = slideImage(slide[index].image)
        slideDescription = slideDescription(slide[index].description)
        setUpConstraints(page: page)
    }
    private func setUpConstraints(page: UIView){
        view.addSubview(slideImage)
        view.addSubview(slideDescription)
        slideImage.translatesAutoresizingMaskIntoConstraints = false
        slideImage.widthAnchor.constraint(equalToConstant: 100).isActive = true
        slideImage.heightAnchor.constraint(equalToConstant: 100).isActive = true
        slideImage.centerXAnchor.constraint(equalTo: page.centerXAnchor).isActive = true
        slideImage.centerYAnchor.constraint(equalTo: page.centerYAnchor,constant: -50).isActive = true
        slideDescription.translatesAutoresizingMaskIntoConstraints = false
        slideDescription.leadingAnchor.constraint(equalTo: page.safeAreaLayoutGuide.leadingAnchor,constant: 80).isActive = true
        slideDescription.trailingAnchor.constraint(equalTo: page.safeAreaLayoutGuide.trailingAnchor,constant: -80).isActive = true
        slideDescription.heightAnchor.constraint(equalToConstant: 60).isActive = true
        slideDescription.centerXAnchor.constraint(equalTo: page.centerXAnchor).isActive = true
        slideDescription.topAnchor.constraint(equalTo: slideImage.bottomAnchor, constant: 24).isActive = true
    }
    private func setUpNextButtonConstraints(){
        view.addSubview(nextButton)
        nextButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        nextButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor,constant: -24).isActive = true
        nextButton.heightAnchor.constraint(equalToConstant: 46).isActive = true
        nextButton.widthAnchor.constraint(equalToConstant: 131).isActive = true
    }
}
//MARK: - UIScrollViewDelegate
extension OnboardingViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        pageControl.currentPage = Int(scrollView.contentOffset.x / scrollView.frame.width )
    }
}
